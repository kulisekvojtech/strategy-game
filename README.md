# Strategy game

Network Communication and Collaborative Data Sharing for Strategy Game on Web Platform.

# Start application without hosting on server

Just open index.html in web browser (For best experience is Chromium or Chrome recommended, IE 11 is not supported.)

Device must be connected to Internet.

# Better time precision

If you want more accurate clock, use https protocol, download a copy of the PeerJS library to your server, change the url to your library copy in index.html and set these http headers:

```
Cross-Origin-Opener-Policy "same-origin"
Cross-Origin-Embedder-Policy "require-corp"
```

.htaccess file with configuration:
```
<IfModule mod_headers.c>
    Header set Cross-Origin-Opener-Policy "same-origin"
    Header set Cross-Origin-Embedder-Policy "require-corp"
</IfModule>
```