
class storage{
	/**
	 * save user name to local storage
	 * 
	 * @param {String} name user name
	 */
	set_user_name(name){
		localStorage.setItem('user_name', name);
	}
	/**
	 * read user name from local storage
	 * 
	 * @returns user name
	 */
	get_user_name(){
		return localStorage.getItem('user_name');
	}

	/**
	 * read profile picture from local storage
	 * 
	 * @returns profile picture in url
	 */
	get_user_profile_picture(){
		return localStorage.getItem('profile_picture');
	}
	/**
	 * save profile picture to local storage
	 * 
	 * @param {String} picture profile picture in url
	 */
	set_user_profile_picture(picture){
		localStorage.setItem('profile_picture', picture);
	}

	/**
	 * read last user uuid from local storage
	 * 
	 * @returns last user uuid
	 */
	get_last_user_uuid(){
		return localStorage.getItem('user_uuid');
	}
	/**
	 * save last user uuid to local storage
	 * 
	 * @param {String} uuid last user uuid
	 */
	set_last_user_uuid(uuid){
		localStorage.setItem('user_uuid', uuid);
	}
	clear_last_user_uuid(){
		localStorage.removeItem('user_uuid');
	}

	/**
	 * read last network uuid from local storage
	 * 
	 * @returns last connected network uuid
	 */
	get_last_network_uuid(){
		return localStorage.getItem('network_uuid');
	}
	/**
	 * save last network uuid to local storage
	 * 
	 * @param {String} uuid 
	 */
	set_last_network_uuid(uuid){
		localStorage.setItem('network_uuid', uuid);
	}

}