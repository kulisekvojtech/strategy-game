/**
 * @file Contains implementation of game map
 * @author Vojtěch Kulíšek
 */

class map{

	/**
	 * create new map
	 * 
	 * @param {Object} stats object with statistics
	 */
	constructor(stats){

		this.generated = false;
		this.map_element = document.createElement("div");
		this.map_element.classList.add("map");
		this.last_size = 0;

		var map_html = "\
		<canvas class='map'></canvas>\
		<div class='container'></div>\
		<div class='dimming-map'>\
			<button class='statistics closed'>Statistics</button>\
		</div>";

		this.map_element.innerHTML = map_html;
		document.body.appendChild(this.map_element);

		// open stats
		this.map_element.getElementsByClassName("dimming-map")[0].getElementsByTagName("button")[0].onclick = function(){

			if(!this.classList.contains("hide")){
				stats.show();
			}
		}

		this.canvas = this.map_element.getElementsByTagName("canvas")[0];
		this.ctx = this.canvas.getContext("2d");

		if(window.screen.height < window.screen.width){
			this.canvas.width = window.screen.height;
			this.canvas.height = window.screen.height;
		} else{
			this.canvas.width = window.screen.width;
			this.canvas.height = window.screen.width;
		}

		this.container = this.map_element.getElementsByClassName("container")[0];

		this.resize();

		var main_object = this;
		window.onresize = function(){
			main_object.resize();
		};
	}
	/**
	 * resize map event
	 */
	resize(){
		var size;
		if(this.map_element.clientWidth > this.map_element.clientHeight){
			size = this.map_element.clientHeight;
		} else{
			size = this.map_element.clientWidth;
		}

		if(this.last_size != size){

			this.container.style.width = size + "px";
			this.container.style.height = size + "px";

			this.last_size = size;
		}
	}
	/**
	 * Active or disable map for interaction
	 * 
	 * @param {Boolean} active_map true value active map
	 */
	active(active_map){

		var map_dimmer = this.map_element.getElementsByClassName("dimming-map")[0];

		if(active_map){
			map_dimmer.classList.add("none");
		} else{
			map_dimmer.classList.remove("none");
		}
	}
	/**
	 * find true bit position in binary
	 * 
	 * @param {Number} number 
	 * @returns position of the true bit
	 */
	binary_one_position(number){
		var pos = 1;
		while(!(number&1)){
			pos += 1;
			number = number >> 1;
		}
		return pos;
	}
	/**
	 * generate game map from seed
	 * 
	 * @param {Number} seed 
	 */
	generate(seed){

		var random = new congruential_generator(seed);
		this.random = random;
		this.map_data = Array();

		for(var i=0;i<81;i+=8){
			this.map_data[i] = Array();
			for(var j=0;j<81;j+=8){
				if(i == 0 || i == 80 || j == 0 || j == 80){
					this.map_data[i][j] = 0;
				} else{
					this.map_data[i][j] = random.next()%255;
				}
			}
		}

		for(var k=4;k>0;k=k>>1){
			for(var i=k;i<81;i+=k*2){
				this.map_data[i] = Array();
				for(var j=0;j<81;j+=k*2){
					this.map_data[i][j] = ((this.map_data[i-k][j]+this.map_data[i+k][j])>>1) + ((random.next()%255)>>this.binary_one_position(k<<2))
										  -(128>>this.binary_one_position(k<<2));
				}
			}
			for(var i=0;i<81;i+=k*2){
				for(var j=k;j<81;j+=k*2){
					this.map_data[i][j] = ((this.map_data[i][j-k]+this.map_data[i][j+k])>>1) + ((random.next()%255)>>this.binary_one_position(k<<2))
										  -(128>>this.binary_one_position(k<<2));
				}
			}
			for(var i=k;i<81;i+=k*2){
				for(var j=k;j<81;j+=k*2){
					this.map_data[i][j] = ((this.map_data[i-k][j]+this.map_data[i+k][j])>>1) + ((random.next()%255)>>this.binary_one_position(k<<2))
										  -(128>>this.binary_one_position(k<<2));
				}
			}
		}

		this.generated = true;
		this.draw();

		// points for company		
		this.container.innerHTML = "";
		this.allowed_positions = Array();
		this.used_points = Array();
		this.company_points = Array();
		for(var i=0;i<160;i++){
			for(var j=0;j<160;j++){

				// position in chunk
				var pos_c_x = i%2;
				var pos_c_y = j%2;
			
				var pos_x = i>>1;
				var pos_y = j>>1;

				if(this.map_data[pos_x][pos_y] > 151 && this.map_data[pos_x+1][j] > 151 &&
				   this.map_data[pos_x][pos_y+1] > 151 && this.map_data[pos_x+1][pos_y+1] > 151 &&
				   this.map_data[pos_x][pos_y] < 236 && this.map_data[pos_x+1][j] < 236 &&
				   this.map_data[pos_x][pos_y+1] < 236 && this.map_data[pos_x+1][pos_y+1] < 236){

					this.allowed_positions.push([i*0.625, j*0.625]);

				} else{

					var depth = this.map_data[pos_x][pos_y]*((2-pos_c_x)*(2-pos_c_y))+
								this.map_data[pos_x+1][pos_y]*((pos_c_x)*(2-pos_c_y))+
								this.map_data[pos_x][pos_y+1]*((2-pos_c_x)*(pos_c_y))+
								this.map_data[pos_x+1][pos_y+1]*((pos_c_x)*(pos_c_y));
					depth >>= 2;
					if(depth > 151 && depth < 236){
						this.allowed_positions.push([i*0.625, j*0.625]);
					}

				}

			}
		}

	}
	/**
	 * draw canvas
	 */
	draw(){

		if(!this.generated){
			return;
		}

		var canvas = this.canvas;
		var ctx = this.ctx;
		var map_data = this.map_data;

		setTimeout(function(){
		(async function(canvas, ctx, map_data){
			var width = canvas.width;
			var height = canvas.height;
			var img = ctx.createImageData(width, height);
			var pixels = img.data;
			var pixel = Array(4).fill(0);

			var ssaa = 4;

			var ssaa_data = 1/ssaa;
			var ssaa_2x = ssaa*ssaa;

			var speed = window.performance.now();

			for(var i=0;i<width;i++){
				for(var j=0;j<height;j++){

					for(var a=0;a<ssaa;a++){
						for(var b=0;b<ssaa;b++){

							var x_raw = (((i+a*ssaa_data)/width)*80);
							var y_raw = (((j+b*ssaa_data)/height)*80);
				
							var x = Math.floor(x_raw);
							var y = Math.floor(y_raw);

							var x_residue = x_raw -x;
							var y_residue = y_raw -y;

							var depth = map_data[x][y]*((1-x_residue)*(1-y_residue)) +
										map_data[x+1][y]*((x_residue)*(1-y_residue)) +
										map_data[x][y+1]*((1-x_residue)*(y_residue)) +
										map_data[x+1][y+1]*((x_residue)*(y_residue));
							
							if(depth < 128){
								pixel[0] += 23;
								pixel[1] += 93;
								pixel[2] += 118;
							}else if(depth < 141){
								pixel[0] += 43;
								pixel[1] += 113;
								pixel[2] += 138;
							}else if(depth < 146){
								pixel[0] += 190;
								pixel[1] += 175;
								pixel[2] += 127;
							}else if(depth < 151){
								pixel[0] += 170;
								pixel[1] += 150;
								pixel[2] += 105;
							}else if(depth < 178){
								pixel[0] += 85;
								pixel[1] += 135;
								pixel[2] += 45;
							}else if(depth < 225){
								pixel[0] += 50;
								pixel[1] += 95;
								pixel[2] += 15;
							}else if(depth < 236){
								pixel[0] += 115;
								pixel[1] += 157;
								pixel[2] += 75;
							}else if(depth < 250){
								pixel[0] += 158;
								pixel[1] += 165;
								pixel[2] += 142;
							}else{
								pixel[0] += 240;
								pixel[1] += 240;
								pixel[2] += 240;
							}
						}
					}

					pixel[0] = Math.round(pixel[0]/ssaa_2x);
					pixel[1] = Math.round(pixel[1]/ssaa_2x);
					pixel[2] = Math.round(pixel[2]/ssaa_2x);

					var index = (i*width+j)*4;
					pixels[index] = pixel[0];
					pixels[index+1] = pixel[1];
					pixels[index+2] = pixel[2];
					pixels[index+3] = 255;

					pixel.fill(0);

				}
			}
			ctx.putImageData(img, 0, 0);

		})(canvas, ctx, map_data);
		}, 16);
	}
	/**
	 * create point on map
	 * 
	 * @param {Number} company_id id of company
	 */
	create_point(company_id){

		var id = this.random.next()%this.allowed_positions.length;
		var original_id = id;
		var passed;
		do{
			passed = true;
			
			id = (id+1)%this.allowed_positions.length;
			// no place on the map
			if(id == original_id){
				return;
			}

			// block hit used points
			for(var i=0;i<this.used_points.length;i++){
				var used_point = this.allowed_positions[this.used_points[i]];
				var new_point = this.allowed_positions[id];
				if((used_point[0]+5 > new_point[0] && used_point[0]-5 < new_point[0]) &&
				   (used_point[1]+2 > new_point[1] && used_point[1]-2 < new_point[1])){
					passed = false;
				}
			}

		}while(this.used_points.includes(id) || !passed);

		this.used_points.push(id);

		// create point element
		var x_y = this.allowed_positions[id];
		var point = document.createElement("div");
		point.classList.add("point");
		point.style.top = (x_y[0]-5) + "%";
		point.style.left = (x_y[1]-1.127) + "%";
		
		point.innerHTML = "\
			<img src='img/map.svg'>\
			<div></div>";

		var list = this.game_control_panel;
		point.getElementsByTagName("div")[0].onclick = function(){

			var parent = this.parentNode;

			if(parent.classList.contains("enemy")){
				list.enemy.blink(company_id);
			} else if(parent.classList.contains("my")){
				list.my.blink(company_id);
			} else{
				list.buy.blink(company_id);
			}
		};

		this.container.appendChild(point);

		this.company_points.push([company_id, point]);
	}
	/**
	 * set point owner
	 * 
	 * @param {Number} company_id id of company
	 * @param {Number} owner owner of company
	 */
	set_point(company_id, owner){

		// company is not on the map
		if(company_id > this.company_points.length-1){
			return;
		}

		if(owner == "my"){
			this.company_points[company_id][1].getElementsByTagName("img")[0].src = "img/map_my.svg";
			this.company_points[company_id][1].className = "point my";
		} else if(owner == "enemy"){
			this.company_points[company_id][1].getElementsByTagName("img")[0].src = "img/map_enemy.svg";
			this.company_points[company_id][1].className = "point enemy";
		} else if(owner == ""){
			this.company_points[company_id][1].getElementsByTagName("img")[0].src = "img/map.svg";
			this.company_points[company_id][1].className = "point";
		}

	}
	/**
	 * hide point on map
	 * 
	 * @param {Number} company_id id of company
	 */
	hide_point(company_id){
		this.company_points[company_id][1].classList.add("hide");
	}
}