/**
 * @file Contains peer network
 * @author Vojtěch Kulíšek
 */

class peer_network{
	constructor(){
		this.network_type = "none";
		this.peer = new Array();
		this.connected = false;
		this.game_time = new game_time(this);
		this.protocol = new protocol(this.game_time, this);
		this.block_new_connections = false;
		this.play_ready = false;
		this.game_staus = "creating";
	}
	/**
	 * set object for statistics
	 * 
	 * @param {Object} stats object with statistics
	 */
	set_stats(stats){
		this.stats = stats;
		this.game_time.set_stats(stats);
	}
	/**
	 * set object for chat
	 * 
	 * @param {Object} chat object with chat
	 */
	set_chat(chat){
		this.chat = chat;
		this.chat.set_protocol_time(this.protocol, this.game_time);
	}
	remove_from_peer_list(peer){
		var pos = this.peer.lastIndexOf(peer);
		this.peer.splice(pos, 1);
		for(var i=0;i<3;i++){
			if(this.peer.length > i){
				this.function_for_list(i+1, this.peer[i].nick_name, this.peer[i].ready, this.peer[i].profile_picture);
				this.send_client_list_all(i+1, this.peer[i].uuid, this.peer[i].nick_name, this.peer[i].ready, this.peer[i].profile_picture);
			}else{
				this.function_for_list(i+1, null, null);
				this.send_client_list_all(i+1, null, null, false, null);
			}
		}
	}
	/**
	 * Set peer remote on all peers
	 * 
	 * @param {Number} id slot number
	 * @param {String} uuid uuid of client for connection
	 * @param {Boolean} ready if is payler ready
	 * @param {String} nickname nick name of client
	 * @param {String} profile_picture profile picture of client
	 */
	send_client_list_all(id, uuid, nickname, ready, profile_picture){
		for(var i=0;i<this.peer.length;i++){
			this.send_client_list(i, id, uuid, nickname, ready, profile_picture);
		}

		if(id != 0){
			if(uuid !== null){
				this.stats.add_peer(id, uuid, nickname, true);
			} else{
				this.stats.remove_peer(id);
			}
		}
	}
	/**
	 * Set peer remote
	 * 
	 * @param {Number} peer_id message for peer with ID
	 * @param {Number} id slot number
	 * @param {String} uuid uuid of client for connection
	 * @param {Boolean} ready if is payler ready
	 * @param {String} nickname nick name of client
	 * @param {String} profile_picture profile picture of client
	 */
	send_client_list(peer_id, id, uuid, nickname, ready, profile_picture){
		var data_to_send = {
			command: "user_set",
			time: this.game_time.safe_is(),
			id: id,
			uuid, uuid,
			nick: nickname,
			ready: ready,
			picture: profile_picture
		}
		var encoded_data = JSON.stringify(data_to_send)

		this.peer[peer_id]["peer"].send(encoded_data);
		this.stats.send(this.get_peer_id(this.peer[peer_id]["peer"]), encoded_data);
	}
	/**
	 * connect to PeerJS signaling server, after successful connection
	 * run ready() function with uuid as parameter
	 */
	connect_signaling_server(){

		// connect to PeerJS signaling server
		var storage_data = new storage();
		var last_uuid = storage_data.get_last_user_uuid();
		if(last_uuid == null){
			this.peerjs = new Peer();
		} else{
			this.peerjs = new Peer(last_uuid);
		}
		peerjs = this.peerjs;
		var main_object = this;
		peerjs.on("open", function(id){
			main_object.uuid = id;

			// save uuid
			storage_data.set_last_user_uuid(id);
			
			main_object.stats.set_uuid(id);

			main_object.ready(id, main_object);
		});

		peerjs.on("connection", function(c){
			var metadata = c.metadata;
			var uuid = c.peer;
			var ready = false;
			var client = new Array();
			var connection_passed = false;
			c.on("open", function(){

				// handle mistaken connections
				if(metadata.app != "6SPVbMUgWt3jxn7M"){
					c.close();
				}

				// if this peer is not server
				if(main_object.network_type != "server"){

					// is peer allowed in network
					var is_not_in = true;
					for(var i=1;i<main_object.peer.length;i++){
						if(main_object.peer[i].uuid == uuid){
							main_object.peer[i].peer = c;
							is_not_in = false;
							connection_passed = true;
							id = i;
						}
					}
					if(is_not_in){
						c.close();
					}
					main_object.stats.is_connection(main_object.get_peer_id(c), true);
					return;
				}

				client["peer"] = c;
				client["uuid"] = uuid;
				client["nick_name"] = metadata.nick_name;
				client["profile_picture"] = metadata.profile_picture;
				client["ready"] = ready;

				// connection limit max 3 player to connect
				if(main_object.peer.length > 2 || main_object.block_new_connections){
					c.close();
				}else{
					connection_passed = true;
					main_object.function_for_list(main_object.peer.length+1, metadata.nick_name, ready, metadata.profile_picture);
					main_object.send_client_list_all(main_object.peer.length+1, uuid, metadata.nick_name, ready, metadata.profile_picture);
					var id = main_object.peer.length;
					main_object.peer.push(client);

					// send information about all peers to another client
					main_object.send_client_list(id, 0, main_object.uuid, main_object.nickname, main_object.play_ready, main_object.profile_picture);
					for(var i=0;i<main_object.peer.length;i++){
						main_object.send_client_list(id, i+1, main_object.peer[i]["uuid"], main_object.peer[i]["nick_name"], main_object.peer[i]["ready"], main_object.peer[i]["profile_picture"]);
					}

					main_object.protocol.new_connection(c);
				}
			});
			c.on("data", function(data){
				var time = main_object.game_time.is();
				main_object.stats.receive(main_object.get_peer_id(c), data, time);
				var encoded = JSON.parse(data);
				if(main_object.network_type == "server"){
					main_object.protocol.server(this, encoded, time);
				} else{
					main_object.protocol.peer(this, encoded);
				}
			});

			// client disconnect
			c.on('close', function(){
				if(connection_passed){

					main_object.stats.is_connection(main_object.get_peer_id(c), false);

					if(main_object.game_staus == "creating"){
						main_object.remove_from_peer_list(client);
						main_object.new_ready();
					}

				}
			});

			// connection crash
			c.peerConnection.oniceconnectionstatechange = function(event) {
				if(c.peerConnection.iceConnectionState === "disconnected"){
					c.close();
				}
			}

		});

		// if disconnected from signaling server
		peerjs.on("disconnected", function(){
			peerjs.reconnect();
		});

		// connecting error
		peerjs.on('error', function(err){

			if(err.type == "server-error" || err.type == "browser-incompatible"){
				var message = new messagebox("game-error");
				message.append();
				message.show();
			}
			if(err.type == "server-error"){
				message.text_s("Unable connect to signaling server");
				return;
			}
			if(err.type == 'browser-incompatible'){
				message.text_s("Your browser is not compatible");
				return;
			}

			storage_data.clear_last_user_uuid();
			main_object.connect_signaling_server();
		});

	}
	/**
	 * set player nick name and profile picture
	 * 
	 * @param {String} nick_name nickname
	 * @param {String} profile_picture profile picture in base64
	 */
	set_profile(nick_name, profile_picture){
		this.nickname = nick_name;
		this.profile_picture = profile_picture;

		this.stats.set_name(nick_name);
	}
	/**
	 * Connect to peer by uuid
	 * 
	 * @param {String} uuid uuid of instance to connect
	 */
	connect(uuid){
		var metadata = {
			reliable: true,
			serialization: "none",
			metadata: {
				app: "6SPVbMUgWt3jxn7M",
				nick_name: this.nickname,
				profile_picture: this.profile_picture
			}
		};
		this.connection = this.peerjs.connect(uuid, metadata);

		var main_object = this;
		this.connection.on('open', function(){
			if(main_object.game_staus == "creating"){
				main_object.game_time.start_sync(main_object.connection);
				main_object.join_game_messagebox.ready_disable(false);
			}
		});

		// only server clients
		this.connection.on("data", function(data){
			var time = main_object.game_time.is();
			main_object.stats.receive(main_object.get_peer_id(main_object.connection), data, time);
			var encoded = JSON.parse(data);

			// peer commands recieve
			if(encoded["command"] == "user_set"){
				var client = new Array();
				client["peer"] = null;
				client["uuid"] = encoded["uuid"];
				client["ready"] = encoded["ready"];
				client["nick_name"] = encoded["nick"];
				client["profile_picture"] = encoded["picture"];
	
				main_object.function_for_list(encoded["id"], client["nick_name"], client["ready"], client["profile_picture"]);
				main_object.peer[encoded["id"]] = client;

				if(main_object.get_network_id() == encoded["id"]){
					main_object.stats.set_id(encoded["id"]);
					main_object.game_time.set_time_type("client");
					main_object.stats.set_time_type("client");

					// set chat
					main_object.chat.set_user(encoded["id"], client["nick_name"]);
					main_object.chat.visible(true);

				} else{
					if(encoded["uuid"] !== null){
						if(encoded["id"] == 0){
							main_object.stats.add_peer(encoded["id"], encoded["uuid"], client["nick_name"], true);
						} else{
							main_object.stats.add_peer(encoded["id"], encoded["uuid"], client["nick_name"], false);
						}
					} else{
						main_object.stats.remove_peer(encoded["id"]);
					}
				}
			} else{
				main_object.protocol.client(this, encoded, time);
			}
		});
		this.peerjs.on('error', function(err){
			if(main_object.network_type == "client"){
				main_object.game_time.stop_sync();
				main_object.messagebox_back();
				main_object.chat.visible(false);
			}
		});

		// server disconnect
		this.connection.on('close', function(){
			
			main_object.game_time.stop_sync();
			if(main_object.game_staus == "creating"){
				main_object.messagebox_back();
				main_object.chat.visible(false);
			}

		});
		var connection = this.connection;
		connection.peerConnection.oniceconnectionstatechange = function(event) {
			if(connection.peerConnection.iceConnectionState === "disconnected"){
				connection.close();
			}
		}
		this.connected = true;
	}
	/**
	 * send to all peers
	 * @param {String} data data to send
	 */
	send_to_all(data){

		var data_to_send = JSON.stringify(data);

		for(var i=0;i<this.peer.length;i++){
			var peer = this.peer[i].peer;
			if(peer != null){
				peer.send(data_to_send);
				this.stats.send(this.get_peer_id(peer), data_to_send);
			}
		}

		// send to server
		if(this.get_network_id() != 0){
			this.connection.send(data_to_send);
			this.stats.send(0, data_to_send);
		}
	}
	/**
	 * send to all clients, without client
	 * 
	 * @param {String} data data to send
	 * @param {NUmber} client exclude this client (network peer id)
	 */
	send_to_all_without(data, client){

		var data_to_send = JSON.stringify(data);
		for(var i=0;i<this.peer.length;i++){
			var peer = this.peer[i].peer;
			if(peer != null && this.get_peer_id(peer) != client){
				peer.send(data_to_send);
				this.stats.send(this.get_peer_id(peer), data_to_send);
			}
		}

	}
	/**
	 * returns network peer id from 0 to 3,
	 * server is alwas 0
	 * 
	 * @returns network peer id
	 */
	get_network_id(){
		if(this.network_type == "server"){
			return 0;
		}
		for(var i=1;i<this.peer.length;i++){
			if(this.peer[i].uuid == this.uuid){
				return i;
			}
		}
	}
	/**
	 * get peer id
	 * 
	 * @param {Object} peer peer about whom we want to find out the id
	 * @returns peer id
	 */
	 get_peer_id(peer){
		if(this.network_type == "server"){
			for(var i=0;i<this.peer.length;i++){
				if(this.peer[i].peer == peer){
					return i+1;
				}
			}
		}
		for(var i=1;i<this.peer.length;i++){
			if(this.peer[i].peer == peer){
				return i;
			}
		}
		return 0;
	}
	get_number_of_peers(){
		var number = 0;

		for(var i=0;i<this.peer.length;i++){
			if(this.peer[i]["uuid"] != null){
				number += 1;
			}
		}

		if(this.network_type == "server"){
			number += 1;
		}
		return number;
	}
	/**
	 * get peer nick name
	 * 
	 * @param {Number} peer_id peer id
	 * @returns nick name
	 */
	get_peer_nick(peer_id){
		if(this.network_type == "server"){
			if(peer_id == 0){
				return this.nickname;
			} else{
				return this.peer[peer_id-1]["nick_name"];
			}
		}
		return this.peer[peer_id]["nick_name"];
	}
	/**
	 * get peer profile picture
	 * 
	 * @param {Number} peer_id peer id
	 * @returns profile picture
	 */
	 get_peer_picture(peer_id){
		if(this.network_type == "server"){
			if(peer_id == 0){
				return this.profile_picture;
			} else{
				return this.peer[peer_id-1]["profile_picture"];
			}
		}
		return this.peer[peer_id]["profile_picture"];
	}
	/**
	 * create peer connection
	 *
	 * @param {Number} id id of peer in peer network 
	 * @param {String} uuid peer to connect
	 * @param {Object} metadata metadata for connection
	 */
	create_peer_connection(id, uuid, metadata){
		
		var main_object = this;

		this.peer[id].peer = this.peerjs.connect(uuid, metadata);
		var connection = this.peer[id].peer;
		connection.on("open", function(){
			main_object.stats.is_connection(main_object.get_peer_id(connection), true);
		});
		connection.on("data", function(data){
			var time = main_object.game_time.is();
			main_object.stats.receive(main_object.get_peer_id(connection), data, time);
			var encoded = JSON.parse(data);
			main_object.protocol.peer(this, encoded);
		});
		connection.on('close', function(){
			main_object.stats.is_connection(main_object.get_peer_id(connection), false);
		});
		connection.peerConnection.oniceconnectionstatechange = function(event) {
			if(connection.peerConnection.iceConnectionState === "disconnected"){
				connection.close();
			}
		}
	}
	/**
	 * connect to all peers
	 */
	connect_to_all(){

		// peer 0 is alwas server
		for(var i=this.get_network_id()+1;i<this.peer.length;i++){
			if(this.peer[i].uuid !== null){
				var metadata = {
					reliable: true,
					serialization: "none",
					metadata: {
						app: "6SPVbMUgWt3jxn7M",
						nick_name: this.nickname,
						profile_picture: this.profile_picture
					}
				}

				this.create_peer_connection(i, this.peer[i].uuid, metadata);

			}
		}
		this.chat.set_type("peer_network");
	}
	/**
	 * crate new game network
	 */
	new_game(stats){
		this.network_type = "server";
		this.new_game_messagebox.next_disable(false);
		this.stats.set_id(0);
		this.stats.set_time_accuracy(0);
		this.stats.set_time_adjust(0);
		this.game_time.set_time_type("server");
		this.stats.set_time_type("server");

		this.chat.visible(true);
		this.chat.set_user(0, this.nickname);
	}
	/**
	 * joins existing game
	 * 
	 * @param {Number} uuid uuid of game to join
	 */
	join_game(uuid){
		this.network_type = "client";
		this.join_game_messagebox.next_disable(false);
		this.join_game_messagebox.ready_disable(true);
		this.connect(uuid);
	}
	/**
	 * send to server i am ready
	 */
	player_ready(){
		if(this.network_type == "server"){
			this.new_game_messagebox.next_disable(true);
			this.play_ready = true;
			this.player_list = this.new_game_messagebox;
			this.protocol.player_list = this.new_game_messagebox;

			this.function_for_list(0, this.nickname, this.play_ready, this.profile_picture);
			this.send_client_list_all(0, this.uuid, this.nickname, this.play_ready, this.profile_picture);
			this.new_ready();

		} else{

			// send ready to server
			var i_am_reday = {
				command: "ready",
				time: this.game_time.safe_is()
			}
			var data_to_send = JSON.stringify(i_am_reday);
			this.connection.send(data_to_send);
			this.stats.send(0, data_to_send);

			this.join_game_messagebox.next_disable(true);
			this.player_list = this.join_game_messagebox;
			this.protocol.player_list = this.join_game_messagebox;
		}
	}
	/**
	 * server call this function, if same player status changed to ready
	 */
	new_ready(){

		// if all players is ready
		var all_ready = true;
		for(var i=0;i<this.peer.length;i++){
			if(!this.peer[i]["ready"]){
				all_ready = false;
			}
		}
		if(all_ready && this.play_ready && this.peer.length > 0){
			this.block_new_connections = true;
			this.start_game();
		}
	}
	/**
	 * Start created game
	 */
	start_game(){

		var game_start_time = this.game_time.is();
		var seed = Math.floor(Math.random()*1000000);
		var duration = this.new_game_messagebox.duration();
		// +10 sec
		game_start_time += 10000000;
		var start_game = {
			command: "start_game",
			time: this.game_time.safe_is(),
			at: game_start_time,
			seed: seed,
			game_duration: duration
		}
		this.network_game_status("running");
		this.send_to_all(start_game);
		new game(this.player_list, this.game_time, game_start_time, seed, duration, this.get_network_id(),
				 this.protocol, this, this.map, this.stats, this.chat);
	}
	/**
	 * set game status
	 * 
	 * @param {String} status game status creating, running or exiting
	 */
	network_game_status(status){
		this.game_staus = status;
	}
	/**
	 * destroy connection to peer network
	 */
	exit_game(){
		if(this.connected){
			this.connection.close();
			this.connected = false;
		}
		for(var i=3;i>=0;i--){
			if(this.peer[i] !== undefined && this.peer[i]["peer"] !== null){
				this.peer[i]["peer"].close();
			}
			this.function_for_list(i, null, null);
		}
		delete(this.peer);
		this.peer = new Array();
		this.network_type = "none";
		this.play_ready = false;
		this.block_new_connections = false;
		this.unblock_exit();

		this.chat.visible(false);
		this.stats.set_id("");
		this.stats.set_name("");
		this.stats.set_time_accuracy("");
		this.stats.set_time_adjust("");
		this.game_time.set_time_type("");
		this.stats.set_time_type("");
		this.stats.remove_all_peers();
	}
}