/**
 * @file Contains input bubble text box class
 * @author Vojtěch Kulíšek
 */

/**
 * creates text bubbles for imputs
 * 
 * example:
 * <div class='input-bubble'> <--- element for constructor parameter
 * 	<div></div>
 * 	<input>
 * </div>
 * 
 * JS:
 * var bubble = document.getElementsByClassName("input-bubble")[0];
 * var input_bubble = new input_bubble();
 * input_bubble.click = function (input){
 * 	return "Example bubble"
 * }
 * input_bubble.append();
 */
class input_bubble {

	/**
	 * create new input bubble text box
	 * 
	 * @param {Object} element parrent div element of input
	 */
	constructor(element){
		this.element = element;
	}
	/**
	 * show text in bouble for 3 seconds
	 * 
	 * @param {String} text text to show  
	 */
	show(text){
		var message_bubble = this.element.getElementsByTagName("div")[0];

		// clear timers
		if(this.timeout_show !== undefined){
			clearTimeout(this.timeout_show);
		}
		if(this.timeout_hide_0 !== undefined){
			clearTimeout(this.timeout_hide_0);
		}
		if(this.timeout_hide_1 !== undefined){
			clearTimeout(this.timeout_hide_1);
		}

		// show
		if(text != ""){
			message_bubble.innerHTML = text;
			message_bubble.classList.remove("none");
			this.timeout_show = setTimeout(function(){
				message_bubble.classList.add("show");
			}, 16);
		}

		// hide
		this.timeout_hide_0 = setTimeout(function(){
			message_bubble.classList.remove("show");
		}, 3000);
		this.timeout_hide_1 = setTimeout(function(){
			message_bubble.classList.add("none");
		}, 3216);

	}
	/**
	 * append bubble
	 */
	append(){
		var input = this.element.getElementsByTagName("input")[0];
		this.input = input;
		var object = this;

		if(this.click !== undefined){
			var text_function = this.click;
			input.onclick = function(){
				var text = text_function(input);
				object.show(text);
			}
		}
		if(this.allowed !== undefined){
			var allowed = this.allowed;
			input.onkeypress = function(e){
				var text = allowed(e.which);
				if(text != ""){
					object.show(text);
					return false;
				}
				return true;
			}
			input.onpaste = function(e){
				var text = e.clipboardData.getData('Text');
				for(var i=0;i<text.length;i++){
					var text_bubble = allowed(text[i].charCodeAt(0));
					if(text_bubble != ""){
						object.show(text_bubble);
					}else{
						this.value += text[i];
					}
				}
				return false;
			}
		}

	}
}