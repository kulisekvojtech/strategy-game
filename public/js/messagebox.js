/**
 * @file Manages message boxes for the game
 * @author Vojtěch Kulíšek
 */

class messagebox{

	/**
	 * Create new message box with given type
	 * @param {String} type "main-menu or new-game",
	 */
	constructor(type) {
		this.type = type;
		this.messagebox_element = document.createElement('div');
		this.messagebox_element.classList.add("messagebox");
		this.messagebox_element.classList.add("closed");

		if(type == "main-menu"){
			this.messagebox_element.classList.add("main-menu");
			this.messagebox_element.innerHTML = "\
			<button title='Profile picture'></button>\
			<input type='file' accept='image/*'>\
			<canvas width='128' height='128'></canvas>\
			<label>\
				Your nick name\
				<input>\
			</label>\
			<div class='bottom-buttons'>\
				<button>New game</button>\
				<button>Join game</button>\
			</div>";
		}else if(type == "new-game" || type == "join-game"){
			this.messagebox_element.classList.add("new-game");
			
			var html = "";

			if(type == "new-game" || type == "join-game"){
				html += "<span>Players</span>";
			}

			for(var i=0;i<4;i++){

				html += "\
				<button class='player-slot'>\
					<div>Empty slot</div>\
					<div class='player'>\
						<img src='img/account.svg'>\
						<span></span>\
					</div>\
				</button>";
			}

			if(type == "new-game"){
				html +="\
				<label>\
					Game time out in minutes\
					<div class='input-bubble'>\
						<div></div>\
						<input type='number' value='15' min='1' max='60'>\
					</div>\
				</label>\
				<label>\
					Invite link\
					<div class='input-bubble'>\
						<div></div>\
						<input readonly>\
					</div>\
				</label>";
			}else if(type == "join-game"){
				this.messagebox_element.classList.add("join-game");
			}

			if(type == "new-game" || type == "join-game"){
				html += "\
				<div class='bottom-buttons'>\
					<button>Leave</button>\
					<button>I am ready</button>\
				</div>";
			}

			this.messagebox_element.innerHTML = html;

			if(type == "new-game"){
				var duration = this.messagebox_element.getElementsByTagName("input")[0];
				var input = this.messagebox_element.getElementsByTagName("input")[1];
				this.set_link = function(link){
					var url = new URL(window.location.href);
					var origin = url.origin;
					if(url.origin == "null"){
						origin = url.protocol + "//";
					}
					input.value = origin+url.pathname+"?uuid="+link;
				}
				this.duration = function(){
					return duration.value;
				}
			}

			var messagebox_element = this.messagebox_element;
			this.set_player = function(id, name, ready, profile_picture){
				var player = messagebox_element.getElementsByClassName("player")[id];
				var name_span = player.getElementsByTagName("span")[0];
				var profile_image = player.getElementsByTagName("img")[0];
				if(name == null){
					player.classList.remove("player-opened");
				} else{
					if(ready){
						name += " ✓";
					}
					name_span.textContent = name;
					profile_image.src = profile_picture;
					player.classList.add("player-opened");
				}
			}
			this.next_disable = function(disable){
				if(type == "new-game"){
					messagebox_element.getElementsByTagName("input")[0].disabled = disable;
				}
				messagebox_element.getElementsByTagName("button")[4].disabled = disable;
				messagebox_element.getElementsByTagName("button")[5].disabled = disable;
			}
			// only for join game
			this.ready_disable = function(disable){
				messagebox_element.getElementsByClassName("bottom-buttons")[0].getElementsByTagName("button")[1].disabled = disable;
			}

		}else if(type == "game-message"){

			this.messagebox_element.classList.add("game-message");

			var input_div = this.messagebox_element;

			/**
			 * set game start messagebox box text
			 * 
			 * @param {string} text text to show in message box
			 */
			this.text_s = function(text){
				input_div.textContent = text;
			}

		}else if(type == "end-game"){

			this.messagebox_element.classList.add("end-game");

			var html = "\
			<div class='focus'>\
				<img class='none'>\
				<span></span>\
				<hr>\
			</div>";

			for(var i=0;i<4;i++){
				html += "\
				<div class='player none'>\
					<span class='position'></span>\
					<img class='none'>\
					<img class='profile_picture'>\
					<span></span>\
				</div>";
			}

			html += "\
			<hr>\
			<button>Close</button>";

			this.messagebox_element.innerHTML = html;

			var messagebox_element = this.messagebox_element;
			this.set_player = function(id, position, i, text, profile_picture){
				var focus_element = messagebox_element.getElementsByClassName("focus")[0];
				var element = messagebox_element.getElementsByClassName("player")[id];
				element.classList.remove("none");

				if(i){
					// set trophy
					if(position < 3){
						var img = focus_element.getElementsByTagName("img")[0];
						img.src = "img/trophy_"+(position+1)+".svg";
						img.classList.remove("none");
					}
					focus_element.getElementsByTagName("span")[0].innerText = (position+1) + ". " + text;
				}

				// set trophy
				if(position < 3){
					var img = element.getElementsByTagName("img")[0];
					img.src = "img/trophy_"+(position+1)+".svg";
					img.classList.remove("none");
				}

				element.getElementsByTagName("img")[1].src = profile_picture;
				element.getElementsByTagName("span")[0].innerText = (position+1) + ".";
				element.getElementsByTagName("span")[1].innerText = text;

			}

			var main_object = this;
			this.messagebox_element.getElementsByTagName("button")[0].onclick = function(){
				main_object.click();
			}

		} else if(type == "game-error"){

			this.messagebox_element.classList.add("game-message");
			this.messagebox_element.classList.add("game-error");

			var div = this.messagebox_element;
			div.innerHTML = "\
				<div></div>\
				<button>Restart application</button>";

			div.getElementsByTagName("button")[0].onclick = function(){
				location.reload();
			}

			/**
			 * set game error messagebox box text
			 * 
			 * @param {string} text text to show in message box
			 */
			this.text_s = function(text){
				div.getElementsByTagName("div")[0].textContent = text;
			}

		}
	}
	/**
	 * Append element to body and add functions
	 */
	append(){
		var type = this.type;
		var profile_picture_data = "img/account.svg";

		if(type == "main-menu"){

			var buttons = this.messagebox_element.getElementsByTagName("button");
			var name = this.messagebox_element.getElementsByTagName("input")[1];
			var profile_picture_input = this.messagebox_element.getElementsByTagName("input")[0];
			var canvas_for_resize = this.messagebox_element.getElementsByTagName("canvas")[0];

			var new_game = this.new_game;
			var join_game = this.join_game;
			/** change profile picture */
			buttons[0].onclick = function(){
				profile_picture_input.click();
			}
			/** New game button */
			buttons[1].onclick = function(){
				new_game(name.value, profile_picture_data);
			}
			buttons[2].onclick = function(){
				join_game(name.value, profile_picture_data);
			}

			// restore name
			var storage_data = new storage;
			var user_name = storage_data.get_user_name();
			if(user_name != null){
				name.value = user_name;
			}
			// save nick name
			name.onchange = function(){
				storage_data.set_user_name(this.value);
			}
			// restore profile picture
			var picture_data = storage_data.get_user_profile_picture();
			if(picture_data != null){
				buttons[0].style.backgroundImage = "url("+picture_data+")";
				profile_picture_data = picture_data;
			}

			/** profile picture resize */
			profile_picture_input.onchange = function(){
				var profile_picture = new Image();
				profile_picture.onload = function(){
					var ctx = canvas_for_resize.getContext('2d');
					ctx.fillStyle = "#fff";
					ctx.fillRect(0, 0, 128, 128);
					ctx.drawImage(this, 0, 0, 128, 128);
					profile_picture_data = canvas_for_resize.toDataURL();
					buttons[0].style.backgroundImage = "url("+profile_picture_data+")";
					storage_data.set_user_profile_picture(profile_picture_data);
				}
				if(this.files.length > 0){
					profile_picture.src = URL.createObjectURL(this.files[0]);
				}
			}

			/**
			 * 
			 * @param {Boolean} is_allowed disable or enable join button
			 */
			this.join = function(is_allowed){
				if(is_allowed){
					buttons[2].disabled = false;
				} else{
					buttons[2].disabled = true;
				}
			}

		}else if(type == "new-game" || type == "join-game"){

			var bottom_buttons = this.messagebox_element.getElementsByClassName("bottom-buttons")[0].getElementsByTagName("button");

			if(type == "new-game"){
				//** Link <input> */
				var input_div = this.messagebox_element.getElementsByClassName("input-bubble")[1];
				var text_bubble = new input_bubble(input_div);
				text_bubble.click = function(input) {
					input.parentNode.parentNode.parentNode.getElementsByTagName("input")[0].focus();
					input.select();
					if (document.execCommand("copy")) {
						return "Link copied";
					}
					return "";
				}
				text_bubble.append();

				/** Game time out <input> */
				var input_div2 = this.messagebox_element.getElementsByClassName("input-bubble")[0];
				var text_bubble_2 = new input_bubble(input_div2);
				text_bubble_2.allowed = function(symbol) {
					if(symbol > "9".charCodeAt(0) || symbol < "0".charCodeAt(0)){
						return "Symbol is not allowed";
					}
					return "";
				}
				text_bubble_2.append();

				// i am ready
				var start_game = this.start;
				bottom_buttons[1].onclick = function(){
					var text = text_bubble_2.input.value;
					for(var i=0;i<text.length;i++){
						if(text[i].charCodeAt(0) > "9".charCodeAt(0) || text[i].charCodeAt(0) < "0".charCodeAt(0)){
							text_bubble_2.show("Symbol is not allowed");
							return;
						}
					}
					if(text_bubble_2.input.value < 1){
						text_bubble_2.show("Minimal allowed time is 1 minute.");
						return;
					}
					if(text_bubble_2.input.value > 60){
						text_bubble_2.show("Maximal allowed time is 60 minutes.");
						return;
					}
					start_game();
				};
			} else{
				bottom_buttons[1].addEventListener("click", this.start);
			}

			bottom_buttons[0].addEventListener("click", this.back);

		}

		document.body.appendChild(this.messagebox_element);
	}
	/**
	 * Show created message box
	 */
	show(){
		this.messagebox_element.classList.remove("closed");
	}
	/**
	 * Hide from screen this message box
	 */
	hide(){
		this.messagebox_element.classList.add("closed");
	}

	/**
	 * remove message box
	 */
	remove(){
		this.messagebox_element.remove();
	}
}