/**
 * @file Contains implementation of game protocol
 * @author Vojtěch Kulíšek
 */

class protocol{

	constructor(time, peer_network){
		this.time = time;
		this.peer_network = peer_network;
	}
	/**
	 * set companies object for enemy buy companies
	 * 
	 * @param {Object} companies 
	 */
	set_companies(companies){
		this.companies = companies;
	}

	/**
	 * new client connection
	 * 
	 * @param {Object} connection new client 
	 */
	new_connection(connection){
		var messages = this.peer_network.chat.get_all_messages();
		for(var i=0;i<messages.length;i++){
			var message = messages[i];
			var data = {
				command: "chat_old",
				time: this.time.is(),
				peer_id: message["peer_id"],
				nick_name: message["nick_name"],
				message_time: message["time"],
				text: message["text"]
			}
			var data_to_send = JSON.stringify(data);
			connection.send(data_to_send);
			this.peer_network.stats.send(this.peer_network.get_peer_id(connection), data_to_send);
		}
	}

	/**
	 * messages intended for server
	 * 
	 * @param {Object} sender sender of message
	 * @param {Array} data message data
	 * @param {Number} time receive time of message
	 */
	server(sender, data, time){

		// time sync
		if(data["command"] == "get_server_time"){
			this.time.send_time_to_client(sender, data, time);
		}

		// chat
		if(data["command"] == "chat_server"){
			var peer_id = this.peer_network.get_peer_id(sender);
			this.send_chat_message_to_client(data["time"], peer_id, data["text"]);
			this.peer_network.chat.receive_message(data["time"], peer_id, this.peer_network.get_peer_nick(peer_id), data["text"]);
		}

		// player ready
		if(data["command"] == "ready"){

			for(var i=0;i<this.peer_network.peer.length;i++){
				var peer = this.peer_network.peer[i];
				if(peer["peer"] == sender){
					peer["ready"] = true;
					this.peer_network.function_for_list(i+1, peer["nick_name"], peer["ready"], peer["profile_picture"]);
					this.peer_network.send_client_list_all(i+1, peer["uuid"], peer["nick_name"], peer["ready"], peer["profile_picture"]);
					this.peer_network.peer[i]["ready"] = true;
					break;
				}
			}
			this.peer_network.new_ready();
		}

		this.peer(sender, data);
	}

	/**
	 * messages intended for client
	 * 
	 * @param {Number} sender sender of message
	 * @param {Array} data message data
	 * @param {Number} received_time received time
	 */
	client(sender, data, received_time){

		// time sync
		if(data["command"] == "server_time"){
			this.time.time_from_server(data, received_time);
		}

		// game start
		if(data["command"] == "start_game"){
			this.peer_network.network_game_status("running");
			this.peer_network.connect_to_all();
			new game(this.player_list, this.time, data["at"], data["seed"], data["game_duration"], this.peer_network.get_network_id(),
					 this, this.peer_network, this.peer_network.map, this.peer_network.stats, this.peer_network.chat);
		}

		this.peer(sender, data);
	}

	/**
	 * messages intended for server or client
	 * 
	 * @param {Number} sender sender of message
	 * @param {Array} data message data
	 */
	peer(sender, data){

		// company commands
		if(data["command"] == "buy"){
			var peer_id = this.peer_network.get_peer_id(sender);
			this.companies.enemy_buy_company(this.peer_network.get_peer_id(sender), data["company_id"], data["time"], this.peer_network.get_peer_nick(peer_id));
		}
		if(data["command"] == "upgrade_lvl"){
			var peer_id = this.peer_network.get_peer_id(sender);
			this.companies.company_buy_next_lvl(data["company_id"], peer_id, data["time"]);
		}
		if(data["command"] == "sell"){
			var peer_id = this.peer_network.get_peer_id(sender);
			this.companies.company_sell(data["company_id"], peer_id, data["time"]);
		}

		// chat
		if(data["command"] == "chat_peer"){
			var peer_id = this.peer_network.get_peer_id(sender);
			this.peer_network.chat.receive_message(data["time"], peer_id, this.peer_network.get_peer_nick(peer_id), data["text"]);
		}
		if(data["command"] == "chat_client"){
			this.peer_network.chat.receive_message(data["message_time"], data["peer_id"], this.peer_network.get_peer_nick(data["peer_id"]), data["text"]);
		}
		if(data["command"] == "chat_old"){
			this.peer_network.chat.receive_message(data["message_time"], data["peer_id"], data["nick_name"], data["text"]);
		}

	}

	/**
	 * send chat text message to all peers
	 * 
	 * @param {Number} time time when message was created
	 * @param {String} text text of message
	 */
	send_chat_message_this_peer(time, text){
		var data = {
			command: "chat_peer",
			time: time,
			text: text
		}
		this.peer_network.send_to_all(data);
	}
	/**
	 * send chat text message to all with architecture client server
	 * 
	 * @param {Number} time time when message was created
	 * @param {String} text text of message
	 */
	send_chat_message_this_server(time, text){
		if(this.peer_network.get_network_id() == 0){
			this.send_chat_message_this_peer(time, text);
		} else{
			var data = {
				command: "chat_server",
				time: time,
				text: text
			}
			var data_to_send = JSON.stringify(data);
			this.peer_network.connection.send(data_to_send);
			this.peer_network.stats.send(this.peer_network.get_peer_id(this.peer_network.connection), data_to_send);
		}
	}
	/**
	 * 
	 * @param {Number} time time when message was created
	 * @param {Number} peer_id peer id of message creator
	 * @param {String} text text of message
	 */
	send_chat_message_to_client(time, peer_id, text){
		var data = {
			command: "chat_client",
			peer_id: peer_id,
			time: this.time.safe_is(),
			message_time: time,
			text: text
		}
		this.peer_network.send_to_all_without(data, peer_id);
	}
	/**
	 * send to all, i buy company
	 * 
	 * @param {Number} company_id id of company to buy
	 * @param {Number} time time of company buy
	 */
	send_buy_company(company_id, time){
		var data = {
			command: "buy",
			company_id: company_id,
			time: time
		}
		this.peer_network.send_to_all(data);
	}
	/**
	 * send to all, i bounght new lvl for my company
	 * 
	 * @param {Number} company_id id of company to buy
	 * @param {Number} time time of lvl upgrade
	 */
	send_company_next_lvl(company_id, time){
		var data = {
			command: "upgrade_lvl",
			company_id: company_id,
			time: time
		}
		this.peer_network.send_to_all(data);
	}
	/**
	 * send to all, i sell my company
	 * 
	 * @param {Number} company_id id of company to buy
	 * @param {Number} time time of lvl upgrade
	 */
	 send_sell_company(company_id, time){
		var data = {
			command: "sell",
			company_id: company_id,
			time: time
		}
		this.peer_network.send_to_all(data);
	}
}