/**
 * @file Contains game implementation
 * @author Vojtěch Kulíšek
 */

class game{

	/**
	 * start new game
	 * 
	 * @param {Object} peer_list messagebox with peer list, before game start
	 * @param {Object} time game synchronized time with peers
	 * @param {Number} start_at game start time
	 * @param {Number} seed seed for congruential generator
	 * @param {Number} game_duration game duration time in minutes
	 * @param {Number} player_id peer id of this peer in network
	 * @param {Object} protocol network protocol
	 * @param {Object} peer_network network protocol
	 * @param {Object} map map object
	 * @param {Object} stats object with statistics
	 * @param {Object} chat object with chat
	 */
	constructor(peer_list, time, start_at, seed, game_duration, player_id, protocol, peer_network, map, stats, game_chat){
		this.start_at = start_at;
		this.peer_list = peer_list;
		this.time = time;
		this.peer_network = peer_network;
		this.game_duration = game_duration;
		this.game_control_panel = new control_panel(stats, game_chat);
		this.random = new congruential_generator(seed);
		this.message = new messagebox("game-message");
		this.protocol = protocol;
		this.game_running = true;
		this.map = map;
		this.player_id = player_id;
		this.map.game_control_panel = this.game_control_panel;
		this.stats = stats;
		this.chat = game_chat;
		this.scoreboard = new messagebox("end-game");
		this.companies = new companies(this.random, start_at, game_duration, time, this.game_control_panel, player_id, protocol, peer_network, seed, this);
		protocol.set_companies(this.companies);

		this.peer_list.hide();
		this.message.append();
		this.message.show();
		this.game_start_time_out(this.start_at-9000000, 10);

		this.map.generate(seed);
	}

	/**
	 * Game start time out
	 * 
	 * @param {Number} time next time to change messagebox
	 * @param {Number} count count of seconds to wait
	 */
	game_start_time_out(time, count){

		var message = this.message;
		var next_time = time-this.time.is();
		var main_object = this;

		var next_time = Math.floor(next_time/1000);
		if(next_time > 1200){
			next_time = 1200;
		}

		setTimeout(function(){

			count -= 1;

			// chat
			if(count == 3){
				main_object.game_control_panel.set_chat_button();
			}

			if(count > 0){
				time += 1000000;
				main_object.game_start_time_out(time, count);
			}else{
				main_object.game_start();
			}

		}, next_time);

		message.text_s("start in "+count+"s");
	}
	/**
	 * start new game
	 */
	game_start(){
		this.chat.game_running(true);
		this.message.hide();
		this.map.active(true);
		this.game_control_panel.show();
		this.stats.control_panel_open();
		this.game_clocks();
		this.companies.show_company_in_time(0);
	}
	/**
	 * game clocks, call this function when game starts
	 */
	game_clocks(){
		var main_object = this;
		var time_to_end = this.game_duration*60000 - (this.time.is()-this.start_at)/1000;

		var time_to_end_in_sec = time_to_end/1000;
		var time_min = Math.floor(time_to_end_in_sec/60);
		var time_sec = Math.floor(time_to_end_in_sec)%60;
		
		// stop game
		if(time_min == 0 && time_sec == 0){
			this.game_stop();
			return;
		}

		if(time_sec > 9){
			this.game_control_panel.set_time(time_min + ":" + time_sec);
		} else{
			this.game_control_panel.set_time(time_min + ":0" + time_sec);
		}

		var next_time = time_to_end%1000;
		if(next_time == 0){
			next_time = 1000;
		}
		setTimeout(function(){
			main_object.game_clocks();
		}, time_to_end%1000);
	}
	/**
	 * show game end score board
	 */
	show_scoreboard(){
		this.scoreboard.append();
		this.chat.game_running(false);
		this.scoreboard.show();
	}
	/**
	 * set data to score board
	 */
	set_scoreboard(){
		var list = new Array();
		var number_of_peers = this.peer_network.get_number_of_peers();

		for(var i=0;i<number_of_peers;i++){
			list[i] = new Array();
			list[i][0] = i;
			list[i][1] = this.companies.money.get_money_in_time(i, -1);
		}
		list.sort(function(a, b){return b[1]-a[1]});

		var position = -1;
		var last_money = -1;
		for(var i=0;i<number_of_peers;i++){
			var is_this_peer = this.player_id == list[i][0];
			if(list[i][1] < last_money || last_money == -1){
				last_money = list[i][1];
				position += 1;
			}
			this.scoreboard.set_player(i, position, is_this_peer, this.peer_network.get_peer_nick(list[i][0]) + " (" + list[i][1] + " €)",
			this.peer_network.get_peer_picture(list[i][0]));
		}
	}
	/**
	 * stop game
	 */
	game_stop(){
		this.companies.stop();
		this.game_control_panel.remove();
		this.message.remove();
		this.map.active(false);

		this.set_scoreboard();
		this.show_scoreboard();
		this.game_running = false;
		this.stats.control_panel_close();

		var main_object = this;

		this.peer_network.network_game_status("end");

		var time_out_runed = false;
		var time_out = setTimeout(function(){
			time_out_runed = true;
			main_object.peer_network.exit_game();
		}, 5000);
		
		this.scoreboard.click = function(){
			if(!time_out_runed){
				clearTimeout(time_out);
				main_object.peer_network.exit_game();
			}
			main_object.scoreboard.remove();
			main_object.peer_network.network_game_status("creating");
			main_object.peer_network.game_menu.show();
		}
		
	}
	/**
	 * upgrade scoreboard
	 */
	upgrade_scoreboard(){
		if(this.game_running == false){
			this.set_scoreboard();
		}
	}
}