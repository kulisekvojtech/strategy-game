/**
 * @file Contains game company_list class
 * @author Vojtěch Kulíšek
 */

class company_list{
	/**
	 * inicialize new company list,
	 * where will be company cards with given type
	 * 
	 * @param {String} type type of list
	 * @param {Object} element element where will be company cards
	 */
	constructor(type, element){
		this.type = type;
		this.element = element;
		this.cards = new Array();
	}
	/**
	 * create new company card and append it
	 * 
	 * @param {Number} id company id
	 * @param {Number} profit earnings of the company
	 * @param {Number} fluctuations fluctuations earnings of the company
	 * @param {Number} lvl level of the company
	 * @param {Number} price price for next level or company buy price
	 * @param {String} owner owner of the company
	 * @returns {Number} id of new item
	 */
	push(id, profit, fluctuations, lvl, price, owner){
		var card = document.createElement('div');
		var item = Array();
		item["card"] = card;
		item["price"] = price;
		this.cards[id] = item;

		var card_html = "\
		<img src='img/lvl"+lvl+".svg'>\
		<div>";

		if(this.type == "enemy"){
			card_html += "\
			<span>Owner: "+owner+"</span>";
		}

		card_html += "\
			<span class='lvl'>Level: "+lvl+"</span>\
			<span class='profit'>Profit: "+profit+" € ± "+fluctuations+" €</span>";

		if(this.type == "buy"){
			card_html += "<span>Price: "+price+" €</span>";
		} else if(price != -1){
			card_html += "<span class='next_lvl_price'>Next level price: "+price+" €</span>";
		}
			
		
		if(this.type == "my"){
			card_html += "\
				<button class='sell'>Sell (50000 €)</button>\
				<button class='upgrade'";
			if(lvl < 3){
				card_html += ">Upgrade</button>";
			} else{
				card_html += " disabled>Upgrade</button>";
			}
				
		} else if(this.type == "buy"){
			card_html += "<button>Buy</button>";
		}
		
		card_html += "\
		</div>\
		";

		card.innerHTML = card_html;
		this.element.appendChild(card);

		var this_class = this;
		if(this.type == "buy"){
			card.getElementsByTagName("button")[0].onclick = function(){
				this_class.button_click(id);
			}
		}else if(this.type == "my"){
			card.getElementsByTagName("button")[0].onclick = function(){
				this_class.button_click_sell(id);
			}
			if(lvl < 3){
				card.getElementsByTagName("button")[1].onclick = function(){
					this_class.button_click(id);
				}
			}
		}

		return this.cards.length-1;
	}
	/**
	 * change company lvl
	 * 
	 * @param {Number} id id of item to remove from list
	 * @param {Number} lvl new level to set
	 * @param {Number} profit earnings of the company
	 * @param {Number} fluctuations fluctuations earnings of the company
	 * @param {Number} price price for next level
	 */
	update_lvl(id, lvl, profit, fluctuations, price){

		var company_card = this.cards[id]["card"];

		this.cards[id]["price"] = price;

		// remove upgrade buy button;
		if(lvl > 2 && this.type == "my"){
			company_card.getElementsByClassName("upgrade")[0].disabled = true;
		}

		if(lvl > 2){
			company_card.getElementsByClassName("next_lvl_price")[0].textContent = "Maximal level";
		} else{
			company_card.getElementsByClassName("next_lvl_price")[0].textContent = "Next level price: " + price + " €";
		}

		company_card.getElementsByClassName("lvl")[0].textContent = "Level: " + lvl;
		company_card.getElementsByClassName("profit")[0].textContent = "Profit: "+profit+" € ± "+fluctuations+" €";
	}
	/**
	 * remove one element from list
	 * 
	 * @param {Number} id id of item to remove from list
	 */
	remove(id){
		this.companies.show_only_once_company(id);
		this.cards[id]["card"].remove();
	}
	/**
	 * remove all companies cards from list
	 */
	remove_all(){
		while(this.cards.length > 0){
			var element = this.cards.pop();
			element["card"].remove();
		}
	}
	/**
	 * update button card status
	 * 
	 * @param {Number} money amount of money
	 */
	update_money(money){

		if(this.type == "buy"){

			// buy companies
			this.cards.forEach(function(card){ 
				if(card["price"] <= money){
					card["card"].getElementsByTagName("button")[0].classList.remove("disabled");
				} else{
					card["card"].getElementsByTagName("button")[0].classList.add("disabled");
				}
			});

		}else if(this.type == "my"){

			// my companies
			this.cards.forEach(function(card){ 
				if(card["price"] <= money && money != -1){
					card["card"].getElementsByTagName("button")[1].classList.remove("disabled");
				} else{
					card["card"].getElementsByTagName("button")[1].classList.add("disabled");
				}
			});
			
		}
	}
	/**
	 * blink company in list
	 * 
	 * @param {Number} id id of item to remove from list
	 */
	blink(id){

		var company_card = this.cards[id]["card"];
		var control_panel_buttons = company_card.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("buttons")[0];
		var button = control_panel_buttons.getElementsByClassName(this.type)[0];
		
		if(button.classList.contains("closed")){
			company_card.classList.add("blink_slow");

			// remove class after animation end
			setTimeout(function(){
				if(company_card.classList.contains("blink_slow")){
					company_card.classList.remove("blink_slow");
				}
			},500);
		} else{
			company_card.classList.add("blink_fast");

			// remove class after animation end
			setTimeout(function(){
				if(company_card.classList.contains("blink_fast")){
					company_card.classList.remove("blink_fast");
				}
			},300);
		}
		
		button.click();
		company_card.parentNode.scrollTop = company_card.offsetTop-6;
	}
}