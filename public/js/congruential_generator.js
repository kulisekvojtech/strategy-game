/**
 * @file Contains congruential generator class
 * @author Vojtěch Kulíšek
 */

class congruential_generator{

	/**
	 * Create new congruent generator
	 * @param {Number} seed seed for new congruent generator
	 */
	 constructor(seed) {
		this.last = 0;
		this.seed = seed+1;
	}

	/**
	 * 
	 * @returns new pseudorandom number
	 */
	next(){
		this.last = ((this.seed*1103515244+12345)*this.last+this.seed)%60000000;
		return this.last;
	}
}