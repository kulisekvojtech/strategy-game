/**
 * @file consistency congruential generator class
 * @author Vojtěch Kulíšek
 */

 class money{
	/**
	 * 
	 * @param {Object} companies array with companies
	 * @param {Object} money_times array with company times of profit
	 * @param {Number} seed seed for congruential generator
	 * @param {Object} companies_o companies
	 */
	constructor(companies, money_times, seed, companies_o){
		this.commands = Array();
		this.money_time = money_times;
		this.companies = companies;
		this.seed = seed;
		this.companies_o = companies_o;
	}
	/**
	 * delete all company level upgrades
	 * 
	 * @param {Number} company_id company id
	 */
	delete_company_lvls(company_id){
		var command = this.commands;
		for(var i=0;i<command.length;i++){
			if(command[i]["type"] == "upgrade" && command[i]["company_id"] == company_id){
				this.commands.splice(i, 1);
			}
		}
	}
	/**
	 * fix consistency model, (not enough money)
	 */
	fix_money_consistency_model(){
		var command = this.commands;

		for(var i=0;i<command.length;i++){
			var command_time = command[i]["time"];

			if(this.get_money_in_time(command[i]["peer_id"], command_time+1) < 0){

				if(command[i]["type"] == "company"){
					var company_id = command[i]["company_id"];
					this.commands.splice(i, 1);
					this.delete_company_lvls(company_id);
					this.companies_o.set_company_owner(company_id, -1, "");
				} else{

					// update control panel
					this.companies_o.down_grade_lvl(command[i]["company_id"], command[i]["peer_id"]);
					this.commands.splice(i, 1);
				}

				return this.fix_money_consistency_model();
			}
		}
	}
	/**
	 * push command to history
	 * 
	 * @param {Number} owner owner of command
	 * @param {Object} company_id id of company to add
	 * @param {Number} time peer network time
	 */
	 push_company_owner(owner, company_id, time){

		var index = this.commands.length-1;
		if(index != -1){
			while(index != -1 && this.commands[index]["time"] > time){
				index -= 1;
			}
			while(index != -1 && this.commands[index]["time"] == time && owner < this.commands[index]["peer_id"]){
				index -= 1;
			}
		}

		var command = Array();
		command["type"] = "company";
		command["time"] = time;
		command["company_id"] = company_id;
		command["peer_id"] = owner;

		this.commands.splice(index+1, 0, command);
	}
	/**
	 * handle consistency company owning problem
	 * 
	 * @param {Number} company_id mistaken owned company
	 */
	dell_company_owner(company_id){

		// delete from commands array
		for(var i=0;i<this.commands.length;i++){
			if(this.commands[i]["type"] == "company" && this.commands[i]["company_id"] == company_id){
				this.delete_company_lvls(this.commands[i]["company_id"]);
				this.commands.splice(i, 1);
			}
		}

		this.fix_money_consistency_model();
	}
	/**
	 * upgrade company lvl
	 * 
	 * @param {Number} company_id mistaken owned company
	 * @param {Number} time peer network time
	 */
	upgrade_company_lvl(company_id, time, owner){

		var index = this.commands.length-1;
		if(index != -1){
			while(index != -1 && this.commands[index]["time"] > time){
				index -= 1;
			}
			while(index != -1 && this.commands[index]["time"] == time && owner < this.commands[index]["peer_id"]){
				index -= 1;
			}
		}

		var command = Array();
		command["type"] = "upgrade";
		command["time"] = time;
		command["company_id"] = company_id;
		command["peer_id"] = owner;

		this.commands.splice(index+1, 0, command);
	}
	/**
	 * sell company
	 * 
	 * @param {Number} player_id player id in peer network
	 * @param {Object} company_id id of company to add
	 * @param {Number} time peer network time
	 */
	sell(player_id, company_id, time){

		var index = this.commands.length-1;
		if(index != -1){
			while(index != -1 && this.commands[index]["time"] > time){
				index -= 1;
			}
			while(index != -1 && this.commands[index]["time"] == time && player_id < this.commands[index]["peer_id"]){
				index -= 1;
			}
		}

		var command = Array();
		command["type"] = "sell";
		command["time"] = time;
		command["company_id"] = company_id;
		command["peer_id"] = player_id;

		this.commands.splice(index+1, 0, command);
	}
	/**
	 * get player money by player id and time
	 * 
	 * @param {Number} player_id player id in peer network
	 * @param {Number} time money in game time
	 * @returns money
	 */
	get_money_in_time(player_id, time){

		var command = this.commands;
		var money = 200000;
		var random = new congruential_generator(this.seed);

		for(var i=0;i<command.length;i++){

			// out of time range
			if(command[i]["time"] > time && time != -1){break;}

			if(command[i]["peer_id"] == player_id){

				var company = this.companies[command[i]["company_id"]];
				var sold = false;

				// sell time
				var sell_time = time;
				for(var j=i+1;j<command.length;j++){

					if(command[j]["time"] >= time && time != -1){break;}

					if(command[j]["company_id"] == command[i]["company_id"] &&
				  	   command[j]["type"] == "sell"){
						sell_time = command[j]["time"];
						sold = true;
						break;
					}
				}

				// company cost
				if(command[i]["type"] == "company"){
					money -= company["lvl0c"];
					if(company.lvl == 1){
						money -= company["lvl1c"];
					}
					if(company.lvl == 2){
						money -= company["lvl2c"];
					}

					// company sold
					if(sold){
						money += 50000;
					}

					// company earnings 0lvl
					var money_gen = this.money_time[command[i]["company_id"]];

					for(var j=0;j<money_gen.length;j++){
						// stop 
						if(money_gen[j] >= sell_time && sell_time != -1){break;}

						if(money_gen[j] > command[i]["time"]){
							money += company["lvl0p"] + (random.next() % (company["fluctuations"]+1));
						}
					}

					this.companies[command[i]["company_id"]].new_lvl = company.lvl;
				} else if(command[i]["type"] == "upgrade"){

					this.companies[command[i]["company_id"]].new_lvl += 1;
					var lvl = this.companies[command[i]["company_id"]]["new_lvl"];
					money -= company["lvl" + lvl + "c"];

					for(var j=0;j<money_gen.length;j++){
						// stop 
						if(money_gen[j] >= sell_time && sell_time != -1){break;}
						if(money_gen[j] > command[i]["time"]){
							money += company["lvl" + lvl + "p"];
							if(lvl > 0){
								money -= company["lvl0p"];
							}
							if(lvl > 1){
								money -= company["lvl1p"];
							}
						}
					}

				}

			}
		}

		return money;
	}
}