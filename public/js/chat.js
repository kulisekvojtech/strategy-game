/**
 * @file Contains chat implementation
 * @author Vojtěch Kulíšek
 */

class chat{

	constructor(){
		this.element = document.createElement('div');
		this.element.classList.add("chat");
		this.element.classList.add("hide");
		this.element.classList.add("not-conected");
		this.button = null;
		this.opened = false;

		this.messages = Array();

		var html = "\
		<div class='messages'>\
			<div></div>\
		</div>\
		<form class='input'>\
			<input placeholder='Chat'>\
			<button type='submit'></button>\
		</form>";

		this.element.innerHTML = html;

		var main_object = this;
		var form = this.element.getElementsByTagName("form")[0];
		form.onsubmit = function(){
			var input = this.getElementsByTagName("input")[0];
			main_object.send_message(input.value);
			input.value = "";
			return false;
		}

		var messages_box = this.element.getElementsByClassName("messages")[0].getElementsByTagName("div")[0];
		form.getElementsByTagName("input")[0].onblur = function(){
			messages_box.scrollTop = messages_box.scrollHeight;
		}

		document.body.appendChild(this.element);

	}

	/**
	 * set player informations
	 * 
	 * @param {Number} peer_network_id id of player in peer network
	 * @param {String} nick_name nick name of this player
	 */
	set_user(peer_network_id, nick_name){
		this.peer_network_id = peer_network_id;
		this.nick_name = nick_name;
	}

	/**
	 * add mesage to list
	 * 
	 * @param {NUmber} time time of message creation
	 * @param {Number} peer_network_id peer nework id of message author
	 * @param {String} nick_name nickname of message author
	 * @param {String} text text of message
	 * @param {Boolean} i_am_sender message is send from this device
	 */
	add_message(time, peer_network_id, nick_name, text, i_am_sender){

		var index = this.messages.length-1;
		if(index != -1){
			while(index != -1 && this.messages[index]["time"] > time){
				index -= 1;
			}
			while(index != -1 && this.messages[index]["time"] == time && peer_network_id < this.messages[index]["peer_id"]){
				index -= 1;
			}
		}

		var message_data = {
			"time": time,
			"peer_id": peer_network_id,
			"nick_name": nick_name,
			"text": text
		};

		var messages = this.element.getElementsByClassName("messages")[0].getElementsByTagName("div")[0];

		var message = document.createElement('p');
		message.textContent = nick_name + ": " + text + " ";

		if(this.messages.length-1 == index){
			messages.appendChild(message);
		} else{
			messages.insertBefore(message, messages.getElementsByTagName("p")[index+1]);
		}
		this.messages.splice(index+1, 0, message_data);
		
		messages.scrollTop = messages.scrollHeight;

		if(this.time.is_synchronized()){
			var time_out = Math.round((time-this.time.is())/1000)+5000;
		} else{
			time_out = 5000;
		}
		setTimeout(function(){
			message.classList.add("old");
		}, time_out);

		if(this.button !== null && !this.opened && !i_am_sender){
			this.button.classList.add("chat-new-message");
		}
	}
	/**
	 * returns all mesages
	 * 
	 * (time, peer_id, nick_name, text)
	 * example x[0]["time"]
	 * 
	 * @returns all messages in chat
	 */
	get_all_messages(){
		return this.messages;
	}

	/**
	 * set protocol object and time object
	 * 
	 * @param {Object} protocol protocol
	 * @param {Object} time time
	 */
	set_protocol_time(protocol, time){
		this.protocol = protocol;
		this.time = time;
	}

	/**
	 * set button from control panel
	 * 
	 * @param {Object} button chat button from control panel
	 */
	set_chat_button(button){
		this.button = button;
	}

	/**
	 * type of network
	 * 
	 * @param {String} type 
	 */
	set_type(type){
		this.type = type;
	}

	/**
	 * set game status
	 * 
	 * @param {Boolean} running status of game 
	 */
	game_running(running){
		if(running){
			this.element.classList.add("game-running");
		} else{
			this.element.classList.remove("game-running");
		}
	}

	/**
	 * send message
	 * 
	 * @param {String} text text of message
	 */
	send_message(text){

		if(text == ""){
			return;
		}

		var time = this.time.is();

		if(this.type == "peer_network"){
			this.protocol.send_chat_message_this_peer(time, text);
		} else{
			this.protocol.send_chat_message_this_server(time, text);
		}
		
		this.add_message(time, this.peer_network_id, this.nick_name, text, true);

		return false;
	}

	/**
	 * receive message
	 * 
	 * @param {Number} time time of new message
	 * @param {Number} peer_id peer network id of sender
	 * @param {String} nick_name nick name of message sender
	 * @param {String} text text of message
	 */
	receive_message(time, peer_id, nick_name, text){
		this.add_message(time, peer_id, nick_name, text, false);
	}

	/**
	 * set chat visibility
	 * 
	 * @param {Boolean} visible if true, chat is visible
	 */
	visible(visible){
		if(visible){
			this.element.classList.remove('not-conected');
		} else{
			this.element.classList.add('not-conected');
			this.clear();
			this.button = null;
			this.set_type("client_server");
		}
	}

	/**
	 * show chat
	 */
	show(){
		this.element.classList.remove('hide');
		this.button.classList.remove("chat-new-message");
		this.opened = true;

		// scroll down
		var messages = this.element.getElementsByClassName("messages")[0].getElementsByTagName("div")[0];
		messages.scrollTop = messages.scrollHeight;
	}
	/**
	 * hide chat
	 */
	hide(){
		this.element.classList.add('hide');
		this.opened = false;
	}

	/**
	 * delete all messages
	 */
	clear(){
		this.messages = Array();
		this.element.getElementsByClassName("messages")[0].getElementsByTagName("div")[0].innerHTML = "";
	}

}