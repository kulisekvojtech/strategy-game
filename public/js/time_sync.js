/**
 * @file Contains time synchronization in peer network
 * @author Vojtěch Kulíšek
 */

class game_time{

	/**
	 * create game time synchronization
	 * 
	 * @param {Object} peer_network peer network
	 */
	constructor(peer_network){
		this.offset = 0;
		this.last_sync_time = 0;
		this.accuracy = -1;
		this.peer_network = peer_network;
		this.type = "";
	}
	/**
	 * set time type (server/client)
	 * 
	 * @param {String} type allowed values: "server", "client", ""
	 */
	set_time_type(type){
		this.type = type;
		if(type == "server"){
			this.accuracy = 0;
		} else{
			this.accuracy = -1;
		}
	}
	/**
	 * returns true if is time synchronized
	 * 
	 * @returns true if is time synchronized
	 */
	is_synchronized(){
		if(this.accuracy == -1 && this.type != "server"){
			return false;
		} else{
			return true;
		}
	}
	/**
	 * set object for statistics
	 * 
	 * @param {Object} stats object with statistics
	 */
	 set_stats(stats){
		this.stats = stats;
		stats.time = this;
	}
	/**
	 * 
	 * @param {Number} offset number that must be added to local monolithic clocks for same time with server
	 * @param {Number} accuracy time synchronize accuracy
	 */
	sync(offset, accuracy){
		if(accuracy < this.accuracy || this.accuracy < 0 ||
		   this.offset+accuracy+10 < offset || this.offset-accuracy-10 > offset){ // detect monolitic clock freez

			// statistics
			if(this.is_synchronized()){
				this.stats.set_time_adjust(offset-this.offset);
			}

			this.offset = offset;
			this.accuracy = accuracy-50;
		}

		// monolitic HW clock run with diferent accuracy
		// reduce accuracy with time
		this.accuracy += 50;
		this.stats.set_time_accuracy(this.accuracy);
	}
	/**
	 * start time sync with server
	 * 
	 * @param {Object} server time server
	 */
	start_sync(server){
		var main_object = this;
		main_object.get_time_from_server(server);
		this.sync_interval = setInterval(function(){
			main_object.get_time_from_server(server);
		}, 1000);
	}
	/**
	 * stop time sync with server
	 */
	stop_sync(){
		clearInterval(this.sync_interval);
		this.accuracy = -1;
	}
	/**
	 * returns synchronized time
	 * 
	 * @returns synchronized game time
	 */
	is(){
		return Math.round(performance.now()*1000) + this.offset;
	}
	/**
	 * returns synchronized time, if time is not synchronized returns -1
	 * 
	 * @returns synchronized game time
	 */
	safe_is(){
		if(this.is_synchronized()){
			return this.is();
		} else{
			return -1;
		}
	}
	/**
	 * Send server time to client for time synchronization
	 * 
	 * @param {Object} client send time to this client
	 * @param {Array} data received data from client
	 * @param {Number} time time of request message recieve
	 */
	send_time_to_client(client, data, time){
		var data_to_send = {
			command: "server_time",
			your_time: data["client_clocks"],
			receive_time: time,
			time: Math.round(performance.now()*1000),
		}
		var data_to_send = JSON.stringify(data_to_send);

		client.send(data_to_send);
		this.stats.send(this.peer_network.get_peer_id(client), data_to_send);
	}
	/**
	 * get time from server (game creator)
	 * 
	 * @param {Object} server game time server
	 */
	get_time_from_server(server){
		var data_to_send = {
			command: "get_server_time",
			time: this.safe_is(),
			client_clocks: this.is()
		}
		var data_to_send = JSON.stringify(data_to_send);

		server.send(data_to_send);
		this.stats.send(0, data_to_send);
	}
	/**
	 * synchronize time from received data
	 * 
	 * @param {Array} data received data from server
	 * @param {Number} received_time received time
	 */
	time_from_server(data, received_time){
		var actual_my_time = received_time;
		var receive_server_time = data["receive_time"];
		var server_time = data["time"];
		var my_last_time = data["your_time"];

		var round_trip = Math.round((actual_my_time-my_last_time)-(server_time-receive_server_time));
		var offset = this.offset+Math.round(server_time+(round_trip/2)-actual_my_time);
		var accuracy = Math.round(round_trip/2);
		this.sync(offset, accuracy);
	}
}