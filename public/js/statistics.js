/**
 * @file Contains statistic implementation
 * @author Vojtěch Kulíšek
 */

class statistic{
	constructor(){
		this.statistics_container = document.createElement('div');
		this.statistics_container.classList.add('statistics-container');
		this.statistics_container.classList.add('closed');

		this.id = 4;

		this.graphs = Array();

		this.uuid_map = Array();

		this.sended = Array(4).fill(0);
		this.received = Array(4).fill(0);

		this.previous_sended = Array(4).fill(0);
		this.previous_received = Array(4).fill(0);

		this.delay_min = Array();
		this.delay_max = Array();
		this.delay_avg = Array(4).fill(0);

		this.without_delay_min = Array();
		this.without_delay_max = Array();
		this.without_delay_avg = Array(4).fill(0);

		this.delay_count = Array(4).fill(0);

		var statistics_html = "";

		statistics_html += "<div>";
		statistics_html += "\
		<div class='player'>\
			<div class='title'>\
				<p>This peer</p><hr>\
			</div>\
			<span>ID: </span><span class='id'></span><br>\
			<span>UUID: </span><span class='uuid'></span><br>\
			<span>Nickname: </span><span class='this_nickname'></span><br>\
			<div class='title'>\
				<p>Time sync</p><hr>\
			</div>\
			<span>Type: </span><span class='time_type'></span><br>\
			<div class='accuracy_graph'>\
				<p>Accuracy</p>\
			</div>\
			<div class='adjust_graph'>\
				<p>Adjust</p>\
			</div>\
			<span>Last adjust: </span><span class='time_adjust'></span><br>\
			<span>Accuracy: </span><span class='time_accuracy'></span><br>\
		</div>\
		";

		for(var i=0;i<3;i++){
			statistics_html += "\
			<div class='player none'>\
				<div class='title'>\
					<p>Remote peer</p><hr>\
				</div>\
				<span>ID: </span><span class='id'></span><br>\
				<span>UUID: </span><span class='uuid'></span><br>\
				<span>Nick name: </span><span class='nickname'></span><br>\
				<span>Status: </span><span class='status'></span><br>\
				<div class='title'>\
					<p>Message delay</p><hr>\
				</div>\
				<div class='delay_graph'>\
					<p>Delay (based on synchronized clocks)</p>\
				</div>\
				<span>All messages</span><br><br>\
				<span>Max: </span><span class='delay_max'>unknown</span><br>\
				<span>Min: </span><span class='delay_min'>unknown</span><br>\
				<span>Avg: </span><span class='delay_avg'>unknown</span><br><br>\
				<span>Without first 5 messages</span><br><br>\
				<span>Max: </span><span class='without_delay_max'>unknown</span><br>\
				<span>Min: </span><span class='without_delay_min'>unknown</span><br>\
				<span>Avg: </span><span class='without_delay_avg'>unknown</span><br><br>\
				<span>Last: </span><span class='delay_last'>unknown</span><br>\
				<div class='title'>\
					<p>Speed</p><hr>\
				</div>\
				<div class='receive_graph'>\
					<p>Download</p>\
				</div>\
				<div class='send_graph'>\
					<p>Upload</p>\
				</div>\
				<span>Download: </span><span class='receive_speed'>0 B/s</span><br>\
				<span>Upload: </span><span class='send_speed'>0 B/s</span><br>\
				<div class='title'>\
					<p>Data</p><hr>\
				</div>\
				<span>Downloaded: </span><span class='received'>0 bytes</span><br>\
				<span>Uploaded: </span><span class='sended'>0 bytes</span><br>\
			</div>";
		}

		statistics_html += "\
		</div>\
		<button></button>\
		";
		this.statistics_container.innerHTML = statistics_html;

		// close button
		var main_object = this;
		this.statistics_container.getElementsByTagName("button")[0].onclick = function(){
			main_object.hide();
		}

		document.body.appendChild(this.statistics_container);

		// graphs
		for(var i=1;i<4;i++){

			var element = this.statistics_container.getElementsByClassName("player")[i];

			this.graphs[i] = Array()
			this.graphs[i]["receive_graph"] = new graph(element.getElementsByClassName("receive_graph")[0]);
			this.graphs[i]["receive_graph"].set_max_values(150);
			this.graphs[i]["receive_graph"].set_axies("Seconds", "B/s");

			this.graphs[i]["send_graph"] = new graph(element.getElementsByClassName("send_graph")[0]);
			this.graphs[i]["send_graph"].set_max_values(150);
			this.graphs[i]["send_graph"].set_axies("Seconds", "B/s");

			this.graphs[i]["delay_graph"] = new graph(element.getElementsByClassName("delay_graph")[0]);
			this.graphs[i]["delay_graph"].set_max_values(150);
			this.graphs[i]["delay_graph"].set_axies("Messages", "ms");

		}

		this.graphs[0] = Array();
		this.graphs[0]["accuracy_graph"] = new graph(this.statistics_container.getElementsByClassName("accuracy_graph")[0]);
		this.graphs[0]["accuracy_graph"].set_max_values(150);
		this.graphs[0]["accuracy_graph"].set_axies("Seconds", "ms");

		this.graphs[0]["adjust_graph"] = new graph(this.statistics_container.getElementsByClassName("adjust_graph")[0]);
		this.graphs[0]["adjust_graph"].set_max_values(150);
		this.graphs[0]["adjust_graph"].set_axies("Count", "ms");

		var main_object = this;
		setInterval(function(){
			if(main_object.id != 4){
				for(var i=0;i<4;i++){
				
					var element = main_object.statistics_container.getElementsByClassName("player")[main_object.get_stats_peer_id(i)];

					if(!element.classList.contains("none") && i != main_object.id){
						var graph = main_object.graphs[main_object.get_stats_peer_id(i)];

						var send = main_object.sended[i] - main_object.previous_sended[i];
						element.getElementsByClassName("send_speed")[0].textContent = send + " B/s";
						graph["send_graph"].add_value(send);
					
						var receive = main_object.received[i] - main_object.previous_received[i];
						element.getElementsByClassName("receive_speed")[0].textContent = receive + " B/s";
						graph["receive_graph"].add_value(receive);
					
						main_object.previous_sended[i] = main_object.sended[i];
						main_object.previous_received[i] = main_object.received[i];
					}
				}
			}
		}, 1000);
	}

	/**
	 * call on control panel open
	 */
	control_panel_open(){
		this.statistics_container.classList.add('panel-open');

		// add green color to button
		if(!this.statistics_container.classList.contains('closed')){
			var stats_buttons = document.getElementsByClassName("statistics");
			for(var i=0;i<stats_buttons.length;i++){
				stats_buttons[i].classList.remove("closed");
			}
		}
	}
	/**
	 * call on control panel close
	 */
	control_panel_close(){
		this.statistics_container.classList.remove('panel-open');
	}

	/**
	 * show statistic
	 */
	show(){
		this.statistics_container.classList.remove('closed');

		// add green color to button
		var stats_buttons = document.getElementsByClassName("statistics");
		for(var i=0;i<stats_buttons.length;i++){
			stats_buttons[i].classList.remove("closed");
		}
	}
	/**
	 * hide statistic
	 */
	hide(){
		this.statistics_container.classList.add('closed');

		// remove green color from button
		var stats_buttons = document.getElementsByClassName("statistics");
		for(var i=0;i<stats_buttons.length;i++){
			stats_buttons[i].classList.add("closed");
		}
	}

	/**
	 * set time type (server/client)
	 * 
	 * @param {String} type allowed values: "server", "client", ""
	 */
	set_time_type(type){
		this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("time_type")[0].textContent = type;
	}
	/**
	 * set accuracy of time sync
	 * 
	 * @param {String} accuracy accuracy in microseconds
	 */
	set_time_accuracy(accuracy){
		if(accuracy === ""){
			this.graphs[0]["accuracy_graph"].clear();
			this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("time_accuracy")[0].textContent = "";
		}else{
			this.graphs[0]["accuracy_graph"].add_value(accuracy/1000);
			this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("time_accuracy")[0].textContent = "±"+(accuracy/1000)+" ms";
		}
	}
	/**
	 * set last adjust of time sync
	 * 
	 * @param {String} adjust adjust in microseconds
	 */
	set_time_adjust(adjust){
		if(adjust === ""){
			this.graphs[0]["adjust_graph"].clear();
			this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("time_adjust")[0].textContent = "";
		}else{
			this.graphs[0]["adjust_graph"].add_value(adjust/1000);
			if(adjust > 0){
				this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("time_adjust")[0].textContent = "+"+(adjust/1000)+" ms";
			} else{
				this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("time_adjust")[0].textContent = (adjust/1000)+" ms";
			}
		}
	}
	/**
	 * set this peer nickname
	 * 
	 * @param {String} nickname nickname of player 
	 */
	set_name(nickname){
		this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("this_nickname")[0].textContent = nickname;
	}
	/**
	 * set this peer id in peer network
	 * 
	 * @param {String} id in peer network
	 */
	set_id(id){
		if(id === ""){
			this.id = 4;
		}else{
			this.id = id;
		}
		
		this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("id")[0].textContent = id;
	}
	/**
	 * set this peer uuid in peer network
	 * 
	 * @param {String} uuid uuid in peer network
	 */
	set_uuid(uuid){
		this.statistics_container.getElementsByTagName("div")[0].getElementsByClassName("uuid")[0].textContent = uuid;
	}

	/**
	 * map id from peer network to stats id
	 * 
	 * @param {Number} id id in peer network  
	 * @returns id in in ststs
	 */
	get_stats_peer_id(id){
		if(this.id < id){
			return id;
		} else{
			return id+1;
		}
	}
	/**
	 * add peer to stats
	 * 
	 * @param {Number} id id of peer in peer network
	 * @param {String} uuid uuid of peer in peer network
	 * @param {String} nickname nickname of peer
	 * @param {Boolean} status status of peer
	 */
	add_peer(id, uuid, nickname, status){
		var element = this.statistics_container.getElementsByClassName("player")[this.get_stats_peer_id(id)];

		element.getElementsByClassName("id")[0].textContent = id;
		element.getElementsByClassName("uuid")[0].textContent = uuid;
		element.getElementsByClassName("nickname")[0].textContent = nickname;

		if(status){
			element.getElementsByClassName("status")[0].textContent = "connected";
		}else{
			element.getElementsByClassName("status")[0].textContent = "disconnected";
		}

		for(var i=0;i<4;i++){
			if(uuid == this.uuid_map[i]){

				this.uuid_map[i] = null;
				this.uuid_map[id] = uuid;

				// recover old data
				this.sended[id] = this.sended[i];
				this.received[id] = this.received[i];
				this.previous_sended[id] = this.previous_sended[i];
				this.previous_received[id] = this.previous_received[i];

				this.delay_min[id] = this.delay_min[i];
				this.delay_max[id] = this.delay_max[i];
				this.delay_avg[id] = this.delay_avg[i];

				this.without_delay_min[id] = this.without_delay_min[i];
				this.without_delay_max[id] = this.without_delay_max[i];
				this.without_delay_avg[id] = this.without_delay_avg[i];

				this.delay_count[id] = this.delay_count[i];

				// graphs
				var old_graphs = this.graphs[this.get_stats_peer_id(i)];

				var graphs = this.graphs[this.get_stats_peer_id(id)];
				graphs["delay_graph"].set_valuse(old_graphs["delay_graph"].get_values());
				graphs["receive_graph"].set_valuse(old_graphs["receive_graph"].get_values());
				graphs["send_graph"].set_valuse(old_graphs["send_graph"].get_values());

				var old_element = this.statistics_container.getElementsByClassName("player")[this.get_stats_peer_id(i)];

				element.getElementsByClassName("delay_avg")[0].textContent = old_element.getElementsByClassName("delay_avg")[0].textContent;
				element.getElementsByClassName("delay_max")[0].textContent = old_element.getElementsByClassName("delay_max")[0].textContent;
				element.getElementsByClassName("delay_min")[0].textContent = old_element.getElementsByClassName("delay_min")[0].textContent;

				element.getElementsByClassName("without_delay_avg")[0].textContent = old_element.getElementsByClassName("without_delay_avg")[0].textContent;
				element.getElementsByClassName("without_delay_max")[0].textContent = old_element.getElementsByClassName("without_delay_max")[0].textContent;
				element.getElementsByClassName("without_delay_min")[0].textContent = old_element.getElementsByClassName("without_delay_min")[0].textContent;

				element.getElementsByClassName("delay_last")[0].textContent = old_element.getElementsByClassName("delay_last")[0].textContent;

				element.getElementsByClassName("receive_speed")[0].textContent = old_element.getElementsByClassName("receive_speed")[0].textContent;
				element.getElementsByClassName("send_speed")[0].textContent = old_element.getElementsByClassName("send_speed")[0].textContent;

				element.getElementsByClassName("received")[0].textContent = old_element.getElementsByClassName("received")[0].textContent;
				element.getElementsByClassName("sended")[0].textContent = old_element.getElementsByClassName("sended")[0].textContent;

				element.classList.remove("none");
				return;
			}
		}

		this.uuid_map[id] = uuid;

		this.sended[id] = 0;
		this.received[id] = 0;
		this.previous_sended[id] = 0;
		this.previous_received[id] = 0;

		this.delay_min[id] = undefined;
		this.delay_max[id] = undefined;
		this.delay_avg[id] = 0;

		this.without_delay_min[id] = undefined;
		this.without_delay_max[id] = undefined;
		this.without_delay_avg[id] = 0;

		this.delay_count[id] = 0;
		
		var graphs = this.graphs[this.get_stats_peer_id(id)];
		graphs["delay_graph"].clear();
		graphs["receive_graph"].clear();
		graphs["send_graph"].clear();

		element.getElementsByClassName("delay_avg")[0].textContent = "unknown";
		element.getElementsByClassName("delay_max")[0].textContent = "unknown";
		element.getElementsByClassName("delay_min")[0].textContent = "unknown";

		element.getElementsByClassName("without_delay_avg")[0].textContent = "unknown";
		element.getElementsByClassName("without_delay_max")[0].textContent = "unknown";
		element.getElementsByClassName("without_delay_min")[0].textContent = "unknown";

		element.getElementsByClassName("delay_last")[0].textContent = "unknown";

		element.getElementsByClassName("receive_speed")[0].textContent = "0 B/s";
		element.getElementsByClassName("send_speed")[0].textContent = "0 B/s";

		element.getElementsByClassName("received")[0].textContent = "0 bytes";
		element.getElementsByClassName("sended")[0].textContent = "0 bytes";

		element.classList.remove("none");
	}
	/**
	 * remove peer from stats
	 * 
	 * @param {Number} id id of peer in peer network 
	 */
	remove_peer(id){
		var element = this.statistics_container.getElementsByClassName("player")[this.get_stats_peer_id(id)];
		this.uuid_map[id] = null;
		element.classList.add("none");
	}
	/**
	 * remove all peers from stats
	 */
	remove_all_peers(){
		for(var i=0;i<3;i++){
			this.remove_peer(i);
		}
		this.uuid_map = Array();
	}

	/**
	 * set connection status
	 * @param {Number} id id of peer in peer network 
	 * @param {Boolean} connection 
	 */
	is_connection(id, connection){
		var element = this.statistics_container.getElementsByClassName("player")[this.get_stats_peer_id(id)];

		if(connection){
			element.getElementsByClassName("status")[0].textContent = "connected";
		}else{
			element.getElementsByClassName("status")[0].textContent = "disconnected";
		}
	}

	/**
	 * sended data
	 * 
	 * @param {Number} id receiver peer id 
	 * @param {String} data sended data
	 */
	send(id, data){
		var element = this.statistics_container.getElementsByClassName("player")[this.get_stats_peer_id(id)];
		this.sended[id] += (new TextEncoder().encode(data)).length;
		element.getElementsByClassName("sended")[0].textContent = this.sended[id] + " bytes";
	}
	/**
	 * received data
	 * 
	 * @param {Number} id sender peer id 
	 * @param {String} data received data
	 * @param {Number} time actual time
	 */
	receive(id, data, time){

		var element = this.statistics_container.getElementsByClassName("player")[this.get_stats_peer_id(id)];

		var decoded_data = JSON.parse(data);
		if(decoded_data["time"] !== undefined && this.time.is_synchronized() && decoded_data["time"] != -1){

			var delay = (time-decoded_data["time"])/1000;

			// avg delay
			this.delay_avg[id] += delay;
			this.delay_count[id] += 1;
			element.getElementsByClassName("delay_avg")[0].textContent = Math.round(this.delay_avg[id]/this.delay_count[id]*1000)/1000 + " ms";

			// without 5 messages
			if(this.delay_count[id] > 5){

				// avg delay
				this.without_delay_avg[id] += delay;
				element.getElementsByClassName("without_delay_avg")[0].textContent = Math.round(this.without_delay_avg[id]/(this.delay_count[id]-5)*1000)/1000 + " ms";

				// max delay
				if(this.without_delay_max[id] === undefined || delay > this.without_delay_max[id]){
					this.without_delay_max[id] = delay;
					element.getElementsByClassName("without_delay_max")[0].textContent = delay + " ms";
				}
				// min delay
				if(this.without_delay_min[id] === undefined || delay < this.without_delay_min[id]){
					this.without_delay_min[id] = delay;
					element.getElementsByClassName("without_delay_min")[0].textContent = delay + " ms";
				}
			}

			// max delay
			if(this.delay_max[id] === undefined || delay > this.delay_max[id]){
				this.delay_max[id] = delay;
				element.getElementsByClassName("delay_max")[0].textContent = delay + " ms";
			}
			// min delay
			if(this.delay_min[id] === undefined || delay < this.delay_min[id]){
				this.delay_min[id] = delay;
				element.getElementsByClassName("delay_min")[0].textContent = delay + " ms";
			}
			
			element.getElementsByClassName("delay_last")[0].textContent = delay + " ms";
			
			var graph = this.graphs[this.get_stats_peer_id(id)];
			graph["delay_graph"].add_value(delay);

		}

		this.received[id] += (new TextEncoder().encode(data)).length;
		element.getElementsByClassName("received")[0].textContent = this.received[id] + " bytes";
	}
}