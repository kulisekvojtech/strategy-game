/**
 * @file Contains game control_panel class
 * @author Vojtěch Kulíšek
 */

class control_panel{
	/**
	 * create new game control panel
	 * 
	 * @param {Object} stats object with statistics
	 * @param {Object} chat object with chat
	 */
	constructor(stats, chat){
		this.opened_menu = "buy";
		this.opened_mobile_menu = false;
		this.opened_chat = false;
		this.chat = chat;
		this.control_panel_element = document.createElement('div');
		this.control_panel_element.classList.add('control_panel');
		this.control_panel_element.classList.add('closed');

		var control_panel_html = "\
		<div class='buttons'>\
			<button class='enemy closed'>Enemy companies</button>\
			<button class='my closed'>My companies</button>\
			<button class='buy'>Buy companies</button>\
			<button class='statistics closed'>Statistics</button>\
			<button class='chat closed'>Chat</button>\
			<button class='companies closed'>Companies</button>\
		</div>\
		<div class='stats'>\
			<div></div>\
			<div></div>\
		</div>\
		<div class='closed lists'>\
			<div class='box buy'>\
				<div class='list enemy'></div>\
				<div class='list my'></div>\
				<div class='list buy'></div>\
			</div>\
		</div>";
	
		this.control_panel_element.innerHTML = control_panel_html;
		document.body.appendChild(this.control_panel_element);

		/** companies show button */
		var list = this.control_panel_element.getElementsByClassName("lists")[0].getElementsByClassName("box")[0];
		list.classList.add("closed");

		/** buttons onclick */
		var this_class = this;
		var companies_button = this.control_panel_element.getElementsByClassName("companies")[0];
		companies_button.onclick = function(){
			if(!this_class.opened_mobile_menu){
				this_class.opened_mobile_menu = true;

				// close chat
				if(this_class.opened_chat){
					this_class.opened_chat = false;
					this_class.chat.hide();
					this_class.control_panel_element.getElementsByClassName("chat")[0].classList.add("closed");
				}

				companies_button.classList.remove("closed");
				list.parentNode.classList.remove("closed");
			} else{
				this_class.opened_mobile_menu = false;
				companies_button.classList.add("closed");
				list.parentNode.classList.add("closed");
			}
		}
		/** chat */
		this.control_panel_element.getElementsByClassName("chat")[0].onclick = function(){
			if(this_class.opened_chat){
				this_class.opened_chat = false;
				this_class.chat.hide();
				this.classList.add("closed");
			} else{
				this_class.opened_chat = true;

				// close company list
				if(this_class.opened_mobile_menu){
					this_class.opened_mobile_menu = false;
					companies_button.classList.add("closed");
					list.parentNode.classList.add("closed");
				}

				this_class.chat.show();
				this.classList.remove("closed");
			}
		}
		this.control_panel_element.getElementsByClassName("buy")[0].onclick = function(){
			this_class.list("buy");
		}
		this.control_panel_element.getElementsByClassName("my")[0].onclick = function(){
			this_class.list("my");
		}
		this.control_panel_element.getElementsByClassName("enemy")[0].onclick = function(){
			this_class.list("enemy");
		}
		/** stats */
		this.control_panel_element.getElementsByClassName("statistics")[0].onclick = function(){
			stats.show();
		}

		/** lists */
		this.buy = new company_list("buy",list.getElementsByClassName("buy")[0]);
		this.my = new company_list("my",list.getElementsByClassName("my")[0]);
		this.enemy = new company_list("enemy",list.getElementsByClassName("enemy")[0]);
	}
	/**
	 * link chat button to chat object
	 */
	set_chat_button(){
		this.chat.set_chat_button(this.control_panel_element.getElementsByClassName("buttons")[0].getElementsByClassName("chat")[0]);
	}
	/**
	 * set companies object
	 * 
	 * @param {Object} companies object with companies
	 */
	set_companies(companies){
		this.buy.companies = companies;
		this.my.companies = companies;
		this.enemy.companies = companies;
	}
	/**
	 * Show created game control panel
	 */
	show(){
		this.control_panel_element.classList.remove('closed');
	}
	/**
	 * remove element
	 */
	remove(){
		this.control_panel_element.classList.add('closed');
		this.control_panel_element.remove();
	}
	/**
	 * 
	 * @param {string} type type of list to show
	 */
	list(type){

		var lists = this.control_panel_element.getElementsByClassName("lists")[0];
		var box = lists.getElementsByTagName("div")[0];
		var buttons = this.control_panel_element.getElementsByClassName("buttons")[0].getElementsByTagName("button");
		var open_button = this.control_panel_element.getElementsByClassName("buttons")[0].getElementsByClassName(type)[0];
		var companies_button = this.control_panel_element.getElementsByClassName("companies")[0];

		/** close all lists */
		for(var i=0;i<3;i++){
			if(buttons[i] != open_button){
				buttons[i].classList.add("closed");
			}
		}

		if(box.classList.contains("buy")){
			box.classList.remove("buy");
		} else if(box.classList.contains("my")){
			box.classList.remove("my");
		} else{
			box.classList.remove("enemy");
		}

		box.classList.add(type);

		if(type != this.opened_menu){
			open_button.classList.remove("closed");
		}

		/** mobile menu open always */
		if(!this.opened_mobile_menu){
			this.opened_mobile_menu = true;

			// close chat
			if(this.opened_chat){
				this.opened_chat = false;
				this.chat.hide();
				this.control_panel_element.getElementsByClassName("chat")[0].classList.add("closed");
			}

			lists.classList.remove("closed");
			companies_button.classList.remove("closed");
		}

		this.opened_menu = type;
	}
	set_time(time){
		var time_element = this.control_panel_element.getElementsByClassName("stats")[0].getElementsByTagName("div")[1];
		time_element.textContent = time;
	}
	set_money(money){
		var money_element = this.control_panel_element.getElementsByClassName("stats")[0].getElementsByTagName("div")[0];
		money_element.textContent = money + " €";
	}
}