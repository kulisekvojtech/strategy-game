/**
 * @file Contains game implementation
 * @author Vojtěch Kulíšek
 */

 class graph{
	constructor(element){
		this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		this.svg.setAttributeNS(null, 'width', '700px');
		this.svg.setAttributeNS(null, 'height', '310px');
		this.svg.setAttributeNS(null, 'viewBox', '0 0 700 310');

		this.values = Array();

		var svg_basic_code = "";

		for(var i=0;i<4;i++){
			var y = 49+i*48;
			var x = 109.6+108.6*i;
			svg_basic_code += "\
				<line x1='1' y1='" + y + "' x2='552' y2='" + y + "' stroke-width='2' stroke='#aaa'/>\
				<line x1='" + x + "' y1='1' x2='" + x + "' y2='250' stroke-width='2' stroke='#aaa'/>\
				<text x='"+x+"' y='273' text-anchor='middle' font-size='24px' class='x-axis-value'>0</text>\
				<text x='553' y='"+(y+7)+"' font-size='24px' class='y-axis-value'>0</text>";
		}

		svg_basic_code += "\
			<polyline fill='none' stroke='#000' stroke-width='2' id='points'/>\
			<rect fill='none' stroke='#000' stroke-width='2' x='1' y='1' width='543' height='241'/>\
			<text x='285.5' y='305' text-anchor='middle' font-size='24px' id='x-axis'>x</text>\
			<text x='0' y='0' text-anchor='middle' font-size='24px' transform='translate(636,120.5) rotate(90)' id='y-axis'>y</text>";

		this.svg.innerHTML = svg_basic_code;
		element.appendChild(this.svg);

	}
	/**
	 * 
	 * @param {String} x x axis description
	 * @param {String} y y axis description
	 */
	set_axies(x, y){
		this.svg.getElementById("x-axis").textContent = x;
		this.svg.getElementById("y-axis").textContent = y;
	}
	/**
	 * set max count of values
	 * 
	 * @param {Number} max max count of values
	 */
	set_max_values(max){
		this.max = max-1;
	}
	/**
	 * draw graph
	 */
	draw(){
		var max_value = this.values[0];
		var min_value = this.values[0];
		for(var i=1;i<this.values.length;i++){
			if(this.values[i] > max_value){
				max_value = this.values[i];
			}
			if(this.values[i] < min_value){
				min_value = this.values[i];
			}
		}

		for(var i=0;i<4;i++){
			this.svg.getElementsByClassName("x-axis-value")[i].textContent = Math.round((this.max/5)*(4-i)*100)/100;
			this.svg.getElementsByClassName("y-axis-value")[i].textContent = Math.round((((max_value-min_value)/5)*(4-i)+min_value)*100)/100;
		}

		var pos = 543;
		var step = (543)/this.max;
		var points = "";
		for(var i=0;i<this.values.length;i++){
			if(max_value-min_value == 0){
				points += pos + "," + (240-0.5*238) + "\n";
			} else{
				points += pos + "," + (240-((this.values[i]-min_value)/(max_value-min_value))*238) + "\n";
			}
			pos -= step;
		}
		this.svg.getElementById("points").setAttributeNS(null, 'points', points);
	}
	/**
	 * add value to graph
	 * @param {Number} value new value for add
	 */
	add_value(value){
		this.values.splice(0, 0, value);
		if(this.values.length > this.max+1){
			this.values.splice(this.values.length-1, 1);
		}

		this.draw();
	}
	/**
	 * get all values from graph
	 * 
	 * @returns all values from graph
	 */
	get_values(){
		return this.values;
	}
	/**
	 * set values to graph
	 * 
	 * @param {Array} values values to apply
	 */
	set_valuse(values){
		this.values = values;
		if(values.length == 0){
			this.clear();
		} else{
			this.draw();
		}
	}
	/**
	 * remove data from graph
	 */
	clear(){
		this.values = Array();
		this.svg.getElementById("points").setAttributeNS(null, 'points', '0 0');
	}
 }