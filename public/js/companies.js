/**
 * @file Contains company management class
 * @author Vojtěch Kulíšek
 */

class companies{
	/**
	 * generate game companies
	 * 
	 * @param {Object} random congruential generator for generating companies
	 * @param {Number} start_at game start time
	 * @param {Number} game_duration game duration time in minutes
	 * @param {Object} time game synchronized time with peers
	 * @param {Object} game_control_panel object for controlling with lists
	 * @param {Number} player_id peer id of this peer in network
	 * @param {Object} protocol network protocol
	 * @param {Object} peer_network peer network
	 * @param {Number} seed seed for congruential generator
	 * @param {Object} game parent game class
	 */
	 constructor(random, start_at, game_duration, time, game_control_panel, player_id, protocol, peer_network, seed, game){

		this.companies = Array();
		this.money_random_add = Array();
		this.commands = Array();
		this.time = time;
		this.random = random;
		this.player_id = player_id;
		this.protocol = protocol;
		this.peer_network = peer_network;
		this.game = game;

		var main_object = this;
		this.game_control_panel = game_control_panel;
		game_control_panel.buy.button_click = function(company){
			main_object.buy_company(company);
		}
		game_control_panel.my.button_click = function(company){
			main_object.company_buy_next_lvl(company, player_id);
		}
		game_control_panel.my.button_click_sell = function(company){
			main_object.company_sell(company, player_id);
		}

		// generate companies
		var company_time = start_at;
		var end_game_time = start_at+game_duration*60000000;
		var company_count = 1;

		while(company_time < end_game_time){

			var lvl = random.next()%3;
			var lvl0p = random.next()%1000;
			var lvl1p = lvl0p + random.next()%1000;
			var lvl2p = lvl1p + random.next()%1000;

			var lvl0c = random.next()%100000+100000;
			var lvl1c = random.next()%10000+10000;
			var lvl2c = random.next()%10000+10000;

			var fluctuations = random.next()%100;
			var company = {
				owner: -1, // company owner, -1 mean copany is for sale otherwise peer network ID is used
				owner_time: -1, // peer network time when 
				time: company_time,
				lvl: lvl, // company start level
				new_lvl: lvl, // actual company lvl
				lvl0p: lvl0p, // level profits
				lvl1p: lvl1p,
				lvl2p: lvl2p,
				lvl0c: lvl0c, // level cost
				lvl1c: lvl1c,
				lvl2c: lvl2c,
				lvl1t: -1,
				lvl2t: -1,
				fluctuations: fluctuations,
				show: false
			}
			this.companies.push(company);
			if(company_count < 7){
				company_time += random.next()%1000000;
				company_count += 1;
			} else{
				company_time += random.next()%30000000;
			}
		}

		this.game_control_panel.set_companies(this);

		// generate times for profits
		var number_of_companies = this.companies.length;
		for(var i=0;i<number_of_companies;i++){

			var company = this.companies[i];
			var profit_time = company["time"]+(random.next()%1000000);
			var add = Array();

			while(profit_time < end_game_time){
				profit_time += random.next()%1000000;
				add.push(profit_time);
			}
			this.money_random_add.push(add);
		}
		this.money = new money(this.companies, this.money_random_add, seed, this);

		// show my money
		var main_object = this;
		var show_money = async ()=>{
			var money = main_object.money.get_money_in_time(main_object.player_id, main_object.time.is());
			main_object.game_control_panel.set_money(money);
			main_object.game_control_panel.buy.update_money(money);
			main_object.game_control_panel.my.update_money(money);
		}
		this.show_money_interval = setInterval(function(){
			show_money();
		}, 16);
	}
	/**
	 * returns next level cost
	 * 
	 * @param {Number} company_id 
	 * @returns next level cost
	 */
	get_company_next_lvl_cost(company_id){
		var company = this.companies[company_id];
		var lvl = company["new_lvl"]+1;
		if(lvl == 3){
			return -1;
		} else{
			return company["lvl" + lvl + "c"];
		}
	}
	/**
	 * returns company buy price
	 * 
	 * @param {Number} company_id 
	 * @returns company buy price
	 */
	get_company_price(company_id){
		var company = this.companies[company_id];
		var cost = 0;

		if(company["lvl"] > -1){
			cost += company["lvl0c"];
			if(company["lvl"] > 0){
				cost += company["lvl1c"];
				if(company["lvl"] > 1){
					cost += company["lvl2c"];
				}
			}
		}
		return cost;
	}
	/**
	 * returns company profit
	 * 
	 * @param {Number} company_id 
	 * @returns company profit
	 */
	get_company_profit(company_id){
		var company = this.companies[company_id];
		return company["lvl" + company["new_lvl"] + "p"];
	}
	/**
	 * show company in cotrol panel only once
	 * 
	 * @param {Number} id id of company to show 
	 */
	show_only_once_company(id){

		var i=id;
		for(;i>-1;i--){
			if(this.companies[i]["show"] != false){
				break;
			}
		}
		i += 1;

		for(var j=i;j<id+1;j++){
			var company_to_show = this.companies[j];

			// company actual profits
			var profit = this.get_company_profit(j);
			var price = this.get_company_price(j);
			company_to_show["show"] = true;
			this.game_control_panel.buy.push(j, profit, company_to_show["fluctuations"], company_to_show["lvl"]+1, price);
			this.game.map.create_point(j);
		}
	}
	/**
	 * show companies in list in their time
	 * 
	 * @param {Number} id id of company to show 
	 */
	 show_company_in_time(id){
		var main_object = this;
		var company_time = Math.floor(this.companies[id]["time"]/1000);

		var length = this.companies.length;

		var time = Math.floor(this.time.is()/1000);
		setTimeout(function(){

			main_object.show_only_once_company(id);

			id += 1;
			if(length > id){
				main_object.show_company_in_time(id);
			}
		}, company_time-time);
	}
	/**
	 * set company owner
	 * 
	 * @param {Number} company_id id of company
	 * @param {Number} player_id id of peer in peer network
	 * @param {String} nick peer nick name
	 */
	set_company_owner(company_id, player_id, nick){

		// remove all lvls form mistaken owner
		this.companies[company_id]["new_lvl"] = this.companies[company_id]["lvl"];

		var company_owner = this.companies[company_id]["owner"];
		var company_to_change = this.companies[company_id];

		var next_price = this.get_company_next_lvl_cost(company_id);
		var price = this.get_company_price(company_id);
		var profit = this.get_company_profit(company_id);

		// remove company from actual owner
		if(company_owner == -1){
			this.game_control_panel.buy.remove(company_id);
		} else if(company_owner == this.player_id){
			this.game_control_panel.my.remove(company_id);
		} else{
			this.game_control_panel.enemy.remove(company_id);
		}
		company_to_change["owner"] = player_id;

		if(player_id == this.player_id){
			this.game_control_panel.my.push(company_id, profit, company_to_change["fluctuations"], company_to_change["lvl"]+1, next_price);
			this.game.map.set_point(company_id, "my");
			this.money.push_company_owner(player_id, company_id, company_to_change["owner_time"]);
		} else if(player_id > -1){
			this.game_control_panel.enemy.push(company_id, profit, company_to_change["fluctuations"], company_to_change["lvl"]+1, next_price, nick);
			this.game.map.set_point(company_id, "enemy");
			this.money.push_company_owner(player_id, company_id, company_to_change["owner_time"]);
		} else{
			this.game_control_panel.buy.push(company_id, profit, company_to_change["fluctuations"], company_to_change["lvl"]+1, price);
			this.game.map.set_point(company_id, "");
		}

		this.game.upgrade_scoreboard();
	}
	/**
	 * buy company
	 * 
	 * @param {Number} company_id id of company
	 */
	buy_company(company_id){
		var company = this.companies[company_id];
		var time = this.time.is();

		if(company["owner_time"] == -1 || company["owner_time"] > time || (company["owner_time"] == time && company[owner] > this.player_id)){

			if(company["owner_time"] != -1){
				this.money.dell_company_owner(company_id);
			}

			// check money
			if(this.money.get_money_in_time(this.player_id, time)-this.get_company_price(company_id) < 0){
				return;
			}

			company["owner_time"] = time;
			this.protocol.send_buy_company(company_id, time);
			this.set_company_owner(company_id, this.player_id);
		}
	}
	/**
	 * enemy buy company
	 * 
	 * @param {Number} enemy_id id of enemy in peer network
	 * @param {Number} company_id id of company
	 * @param {Number} time time of enemy buy company
	 * @param {String} nick enemy nick name
	 */
	enemy_buy_company(enemy_id, company_id, time, nick){
		var company = this.companies[company_id];
		if(company["owner_time"] == -1 || company["owner_time"] > time || (company["owner_time"] == time && company[owner] > enemy_id)){

			// check money
			if(this.money.get_money_in_time(enemy_id, time)-this.get_company_price(company_id) < 0){
				return;
			}

			if(company["owner_time"] != -1){
				this.money.dell_company_owner(company_id);
			}

			company["owner_time"] = time;
			this.set_company_owner(company_id, enemy_id, nick);
		}
	}
	/**
	 * sell company
	 * 
	 * @param {Number} company_id id of company
	 * @param {Number} player_id id of peer in peer network (company owner)
	 * @param {Number} time time of enemy upgrade company lvl
	 */
	company_sell(company_id, player_id, time){
		
		var call_time = this.time.is();
		this.game.map.hide_point(company_id);

		// remove company
		if(player_id == this.player_id){
			this.game_control_panel.my.remove(company_id);
			this.money.sell(player_id, company_id, call_time);
			this.protocol.send_sell_company(company_id, call_time);
		} else{
			this.game_control_panel.enemy.remove(company_id);
			this.money.sell(player_id, company_id, time);
		}

	}
	/**
	 * buy next lvl
	 * 
	 * @param {Number} company_id id of company
	 * @param {Number} player_id id of peer in peer network (company owner)
	 * @param {Number} time time of enemy upgrade company lvl
	 */
	company_buy_next_lvl(company_id, player_id, time){

		var call_time = this.time.is();
		var company = this.companies[company_id];
		var new_lvl = company["new_lvl"]+1;

		if(company["owner"] == player_id){

			if(player_id == this.player_id){

				// check money
				if(this.money.get_money_in_time(player_id, call_time)-company["lvl" + new_lvl + "c"] < 0){
					return;
				}
				this.companies[company_id]["new_lvl"] += 1;

				this.money.upgrade_company_lvl(company_id, call_time, player_id);
				this.protocol.send_company_next_lvl(company_id, call_time);
				this.game_control_panel.my.update_lvl(company_id, new_lvl+1, this.get_company_profit(company_id), company["fluctuations"],
					this.get_company_next_lvl_cost(company_id));
				this.companies[company_id]["lvl" + company["new_lvl"] + "t"] = call_time;
			} else{

				// check money
				if(this.money.get_money_in_time(player_id, time)-company["lvl" + new_lvl + "c"] < 0){
					return;
				}
				this.companies[company_id]["new_lvl"] += 1;

				this.money.upgrade_company_lvl(company_id, time, player_id);
				this.game_control_panel.enemy.update_lvl(company_id, new_lvl+1, this.get_company_profit(company_id), company["fluctuations"],
				this.get_company_next_lvl_cost(company_id));
				this.companies[company_id]["lvl" + company["new_lvl"] + "t"] = time;
			}
	
			this.game.upgrade_scoreboard();

		}
	}
	/**
	 * down grade lvl
	 * 
	 * @param {Number} company_id id of company
	 * @param {Number} player_id id of peer in peer network (company owner)
	 */
	down_grade_lvl(company_id, player_id){

		var company = this.companies[company_id];
		var new_lvl = company["new_lvl"]-1;
		this.companies[company_id]["new_lvl"] -= 1;

		if(player_id == this.player_id){
			this.game_control_panel.my.update_lvl(company_id, new_lvl+1, this.get_company_profit(company_id), company["fluctuations"],
				this.get_company_next_lvl_cost(company_id));
		} else{
			this.game_control_panel.enemy.update_lvl(company_id, new_lvl+1, this.get_company_profit(company_id), company["fluctuations"],
				this.get_company_next_lvl_cost(company_id));
		}
	}
	/**
	 * stop game
	 */
	stop(){
		clearInterval(this.show_money_interval);
	}
}