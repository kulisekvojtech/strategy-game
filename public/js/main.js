/**
 * @file Contains main javascript
 * @author Vojtěch Kulíšek
 */

/**
 * 
 * @returns uuid of invited game
 */
function get_uuid(){
	var url = new URL(window.location.href);
	var url_uuid = url.searchParams.get("uuid");

	// storage
	var data = new storage();
	if(url_uuid == null){
		return data.get_last_network_uuid();
	} else{
		data.set_last_network_uuid(url_uuid);
	}

	return url_uuid;
}

function main(uuid, game_network){
	
	var block_exit = false;
	game_network.unblock_exit = function(){
		block_exit = false;
	}

	/** invited game */
	var game_uuid = get_uuid();

	/** create new chat */
	var game_chat = new chat();
	game_network.set_chat(game_chat);

	/** create new game map */
	var game_map = new map(game_network.stats);
	game_map.generate(Math.floor(Math.random()*60000000));
	game_network.map = game_map;

	/** create game main menu */
	var game_menu = new messagebox("main-menu");
	game_network.game_menu = game_menu;

	game_menu.new_game = function(name, profile_picture){
		game_menu.hide();
		new_game.set_player(0, name, false, profile_picture);
		game_network.set_profile(name, profile_picture);
		game_network.function_for_list = new_game.set_player;
		game_network.new_game_messagebox = new_game;
		block_exit = true;
		game_network.new_game();
		new_game.show();
	}
	game_menu.join_game = function(name, profile_picture){
		game_menu.hide();
		game_network.set_profile(name, profile_picture);
		game_network.function_for_list = join_game.set_player;
		game_network.messagebox_back = join_game.back;
		game_network.join_game_messagebox = join_game;
		game_network.messagebox_connected = join_game.enable;
		block_exit = true;
		game_network.join_game(game_uuid);
		join_game.show();
	}
	game_menu.append();

	/** crate new game message box */
	var new_game = new messagebox("new-game");
	new_game.set_link(uuid);
	new_game.back = function(){
		new_game.hide();
		game_network.exit_game();
		block_exit = false;
		game_menu.show();
	}
	new_game.start = function(){
		game_network.player_ready();
	}
	new_game.append();

	if(game_uuid !== null){
		var join_game = new messagebox("join-game");
		join_game.back = function(){
			join_game.hide();
			join_game.ready_disable(true);
			game_network.exit_game();
			block_exit = false;
			game_menu.show();
		}
		join_game.start = function(){
			game_network.player_ready();
		}
		join_game.append();
		game_menu.show();
	} else{
		game_menu.join(false);
		game_menu.show();
	}

	// block closing page if game is running
	window.onbeforeunload = function(e){
		if(block_exit){
			e.preventDefault();
			e.returnValue = 'Please leave game before exiting';
		}
	}
}

window.onload = function(){

	/** init network */
	var first_ready = true;
	var game_network = new peer_network();

	/** create new statistic */
	var stats = new statistic();
	game_network.set_stats(stats);

	game_network.ready = function(uuid, obj){

		if(first_ready){
			main(uuid, obj);
			first_ready = false;
		}
		
	}
	game_network.connect_signaling_server();

}